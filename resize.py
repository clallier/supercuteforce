#import require libraries
from PIL import Image
import glob
import os
 
imList=glob.glob("*.png")

#Loop through all the image:
for img in imList:
	# open the image
	im = Image.open(img)
	print "resizing:",os.path.basename(img)
    #new image width and height
	size2=(im.size[0]*2, im.size[1]*2)
	out = im.resize(size2)
	# extract the filename and extension from path
	fileName, fileExt = os.path.splitext(img)
	out.save(fileName+'.png', "PNG")