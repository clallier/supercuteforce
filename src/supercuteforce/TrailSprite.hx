package supercuteforce;

import flambe.display.Sprite;
import flambe.display.ImageSprite;
import flambe.Entity;
import flambe.display.BlendMode;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class TrailSprite extends Sprite
{
	var trailElements:Array<Sprite>;
	var current:Int;
	var period0:Int;
	var period:Int;
	var alpha0:Float;
	
	public function new( texture:String, blend:BlendMode, count:Int, period:Int, alpha:Float ) 
	{
		super();
		trailElements = new Array<Sprite>();
		this.current = 0;
		this.alpha0 = alpha;
		this.period0 = period;
		this.period = 0;
		
		// caching elements
		for (i in 0...count) {
			var s = new ImageSprite(GV.tPack.getTexture(texture));
			trailElements.push(s);
			trailElements[i].blendMode = blend;
			trailElements[i].visible = false;
		}

	}
	
	override public function onAdded() {
		for (i in 0...trailElements.length) {
			if (owner != null) {
				owner.addChild(new Entity().add(trailElements[i]));
			}
		}
	}
	
	public function addElement(x:Float, y:Float, scale:Float, angleDeg:Float = 0) {
		period++;
			
		trailElements[current].alpha._ = alpha0;
		trailElements[current].setXY(x, y);
		trailElements[current].scaleX.animateTo(scale, .1);
		trailElements[current].scaleY.animateTo(scale, .1);
		trailElements[current].rotation._ = angleDeg;
		trailElements[current].visible = true;
		//trace("added "+ current, "x:"+trailElements[current].x._, "a:"+trailElements[current].alpha._);
			
		if (period == period0) {
			updateElements();
			period = 0;
			current++;
		}
		
		if (current >= trailElements.length) {
			current = 0;
		}
	}
	
	public function updateElements() {
		var li:Float = trailElements.length;
		//trace("update " +current);
		
		
		for (i in 0...trailElements.length) {
			var idx:Float = current -i;
			if(i>current) idx = li + idx;
				
			var alpha:Float = alpha0 * (li - idx) / li;
			trailElements[i].alpha._ = alpha;
			//trace("upped idx:" + idx +" x:" + trailElements[i].x._+" a:" +trailElements[i].alpha._);
		}
	}
	override public function onRemoved() {
		super.onRemoved();
	}
	
	override public function dispose() {
		super.dispose();
	}
}