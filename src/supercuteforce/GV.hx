package supercuteforce;

import flambe.asset.AssetPack;
import flambe.display.EmitterMold;
import flambe.display.FillSprite;
import flambe.display.Font;
import flambe.display.Sprite;
import flambe.display.TextSprite;
import flambe.Entity;
import flambe.scene.Director;
import flambe.scene.Transition;
import flambe.sound.Sound;
import flambe.System;
import haxe.Json;
import flambe.display.Orientation; 
import supercuteforce.gui.Button;
import flambe.util.Promise;
import flambe.asset.Manifest;
import flambe.sound.Playback;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class GV
{
	public static var version = "1.1.23";
	// assets 
	public static var font:Font;
	public static var dlgfont:Font;
	public static var bPack:AssetPack;
	public static var tPack:AssetPack;
	public static var mPack:AssetPack;
	public static var mold:EmitterMold;
	public static var smallMold:EmitterMold;
	static var dialogs:Dynamic;
	
	// max types
	public static var max_types = 6;
	
	public static var viewport:Sprite;
	public static var x:Float; // viewport x in px
	public static var y:Float;// viewport y in px
	
	// screen size
	public static var k:Int = 32;
	public static var desktopMode:Bool = true;
	public static inline var mapWidth:Int = 9; 	// map width in tiles
	public static inline var mapHeight:Int = 15;	// map height in tiles
	public static var width:Int = k * (mapWidth+2); // 352: viewport width in px
	public static var height:Int = k * (mapHeight+2);// 544;// viewport height in px
	
	// storage keys
	public static var keyLevel:String = "level";
	public static var keyHasGun:String = "hasGun";
	public static var keyEasy:String = "easy";
	public static var keyMedium:String = "medium";
	public static var keyHard:String = "hard";
	public static var keyVHard:String = "vhard";
	public static var keySoundsOn:String = "soundsOn";
	
	// rotation message
	public static var turnMessage:Entity;
	public static var showTurnMessage:Bool = false;
	
	// images
	public static var imgGodsCute:String = "kawai_gods";
	public static var imgGodsCut2:String = "kawai_gods2";
	public static var imgPrincess:String = "EmeraldPrincess";
	public static var imgKawaiGun:String = "kawai_gun";
	public static var imgMaskedMa:String = "masked_god";
	public static var imgLuckyGod:String = "lucky_god";
	public static var imgTheGifts:String = "gift";
	public static var imgTransOff:String = "transponder_off";
	public static var imgTranspOn:String = "transponder_on";
	
	// names
	public static var nameGodsCute:String;
	public static var namePrincess:String;
	public static var nameNewItems:String;
	public static var nameMaskedMa:String;
	public static var nameLuckyGod:String;
	public static var nameTranspon:String;
	
	// music
	public static var finishedLoadMusic:Bool = false;
	
	public function new() 
	{	
		initViewport();
				
		if (!GV.getRoot().has(Director))
            GV.getRoot().add(new Director().setSize(GV.width, GV.height));
			
		if (System.storage.get(GV.keySoundsOn, 1.0) == 0)
			soundSwitch();
	}
	
	public static function getRoot() :Entity
	{
		return GV.viewport.owner;
	}
	
	public static function unwindToScene (scene :Entity, ?trans :Transition)
    {
        var d:Director = GV.getRoot().get(Director);
		if (d != null) {
			//trace("pushing scene");
			d.unwindToScene(scene, trans);
		}
    }

	public static function setBPack (pack :AssetPack)
    {
        GV.bPack = pack;
		
		var dialogDatas = pack.getFile("dialogs_en.json").toString();
		if(dialogDatas.length != 0)
			GV.dialogs = Json.parse(dialogDatas);
		
		nameGodsCute = getText(310);
		namePrincess = getText(311);
		nameNewItems = getText(312);
		nameMaskedMa = getText(313);
		nameLuckyGod = getText(314);
		nameTranspon = getText(315);
    }
	
	public static function setTPack (pack :AssetPack)
    {
        GV.tPack = pack;
		GV.font = new Font(pack, "tiny");
		GV.dlgfont = new Font(pack, "ps2p");
		GV.mold = new EmitterMold(pack, "particle");
		GV.smallMold = new EmitterMold(pack, "particle_small");
	}
	
	public static function startLoadingMusicPack () {
		var manifestMusics = Manifest.fromAssets("musics", false);
		var loaderMusics = System.loadAssetPack(manifestMusics);
		loaderMusics.get(onFinishLoadMusics);
	}
	
	static function onFinishLoadMusics (pack :AssetPack) {
		GV.mPack = pack;
		finishedLoadMusic = true;
	}
	
	public static function resize() {
		
		computeViewportMaxSize();
		centeringViewport();
		
		var w = System.stage.width;
		var h = System.stage.height;
		
		//turn device message
		if(desktopMode == false && w >= h && GV.showTurnMessage == false) {
			var d:Director = GV.getRoot().get(Director);
			if (d != null) {
				var btn = new Button("btn_retry", "btn_center", "btn_right", getText(332));
				turnMessage = new Entity().add(btn);
				d.pushScene(turnMessage);
				btn.setXY(GV.width*0.5, GV.height*0.5);
				
				GV.showTurnMessage = true;
				trace("showing rotation message");
			}
		}
		
		else if (GV.showTurnMessage == true && w < h) {
			var d:Director = GV.getRoot().get(Director);
			if (d != null) {
				d.popScene();
				if(turnMessage != null)
					turnMessage.dispose();
				GV.showTurnMessage = false;
				trace("stop showing rotation message");
			}
		}
	}	

	public static function initViewport() {
		// Orientation
		trace("orientation  : " + System.stage.orientation);
		if(System.stage.orientation != null && System.stage.orientation._ == Landscape)
			System.stage.lockOrientation(Portrait);
		trace("orientation  : " + System.stage.orientation);
		
		
		// try to request fs
		trace("fullscreen : " + System.stage.fullscreen +", isSupported ? " + System.stage.fullscreenSupported);
		if(System.stage.fullscreen._ == false && System.stage.fullscreenSupported)
			System.stage.requestFullscreen(true);
		trace("fullscreen : " + System.stage.fullscreen);
		
		trace("touch sys : " + System.touch.supported);
		if (System.touch.supported == false)
			desktopMode = true;
 		else desktopMode = false;
		
#if flash
		desktopMode = true;
#elseif android || ios		
		desktopMode = false;
#end
		trace("desktop Mode? " + desktopMode);

		// 1 mob/tablet mode 
		// try to request fs + try to get orientation setted to portrait
		// width et height => real sys.stage.w && real sys.stage.w 
		// background has to get the whole w and h
		// center the drawing zone 
		// has to show rotation message
	
		// 2 desktop mode => show in Landscape
		// maximize height and adjust width to fit it
		// bg has to fit the max height and calculated width
		// center the drawing zone
		// no rotation message
				
		computeViewportMaxSize();
	
		var gameRatio = computeGameRatio();
		var currentRatio = GV.width / GV.height;
		trace('gameRatio : ' + gameRatio + " currentRatio : " + currentRatio);

		var k = 32;
			
		// desktopMode => fit to height + limit width 
		if (currentRatio > gameRatio || desktopMode ) {
			k = Std.int(GV.height / (mapHeight + 2));
			trace('should fit in height : ' + k);
		}
		// fit in width 
		else {
			k = Std.int(GV.width / (mapWidth + 2));
			trace('should fit in width : ' + k);
		}
	
		//base game resolution
		if (k >= 128) GV.k = 128;
		else if (k >= 64) GV.k = 64;
		else GV.k = 32;
		trace('base size is : ' +GV.k);
		
		GV.viewport = new FillSprite(0xcccccc, GV.width, GV.height);
		System.root.addChild(new Entity().add(GV.viewport));
		trace('creating viewport : ' + GV.width + ", " + GV.height);
		
		centeringViewport();
	}
		
	public static function scaleTo(wantedSizeInPix:Float, naturalSize:Float) : Float 
	{
		return (1.0 / naturalSize * wantedSizeInPix);
	}
	
	public static function getText(i:Int) {
		if (dialogs == null) return "";
		var txt = Reflect.field(dialogs, "" + i);
		return txt;
	}
	
	public static function soundSwitch() 
	{
		
		if (System.volume._ == 0) {
			System.volume.animateTo(1, 1);
			System.storage.set(GV.keySoundsOn, 1);
		}
		else {
			System.volume.animateTo(0, 1);
			System.storage.set(GV.keySoundsOn, 0);
		}
	}
	
	static private function computeViewportMaxSize():Void 
	{
		var h = System.stage.height;
		var w = System.stage.width;
		trace('original stage size : ' + w + ", " + h);
		
		// if user starts game in landscape
		if (desktopMode == false && w > h) {
			var temp = w;
			w = h;
			h = temp;
			trace('starts in landscape : switching w and h');
		}
		
		//if(desktopMode) {
		//	w =	Std.int(h * computeGameRatio());
		//	trace('desktopMode : force w to ' + w);
		//}
		
		// saving size
		GV.width = w;
		GV.height = h;
		trace('finally stored viewport size : ' + w + ", " + h);
	}
	
	static private function centeringViewport():Void 
	{
		GV.viewport.x._ = Std.int(0.5 * System.stage.width - 0.5 * GV.width);
		GV.viewport.y._ = Std.int(0.5 * System.stage.height - 0.5 * GV.height);	
		GV.x = GV.viewport.x._;
		GV.y = GV.viewport.y._;
		trace('centering viewport : ' + GV.x + ", " + GV.y);
	}
	
	static private function computeGameRatio():Float
	{
		return (GV.mapWidth / GV.mapHeight);
	}
}