package supercuteforce;

import flambe.animation.AnimatedFloat;
import flambe.Component;
import flambe.display.ImageSprite;
import flambe.display.Texture;
import flambe.Entity;
import flambe.input.Key;
import flambe.input.KeyboardEvent;
import flambe.input.PointerEvent;
import flambe.platform.KeyCodes;
import flambe.System;
import flambe.animation.Ease;
import flambe.asset.AssetPack;
import supercuteforce.scene.GameScene;
import supercuteforce.GV;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Player extends ImageSprite
{
	var scene:GameScene;
	var t_noweapon_1:Texture;
	var t_noweapon_2:Texture;
	var t_weapon_1:Texture;
	var t_weapon_2:Texture;
	var animTimer:Float = 1;
	var isInSlowMode:Bool = false;
	var canShoot:Bool = false;
	
	public static var hasGun(default, null):Bool;
	
	public function new(scene:GameScene) {
		this.scene = scene;
		hasGun = System.storage.get(GV.keyHasGun, false);
		t_noweapon_1 = GV.tPack.getTexture("EmeraldPrincess_noweapon_1");
		t_noweapon_2 = GV.tPack.getTexture("EmeraldPrincess_noweapon_2");
		t_weapon_1 = GV.tPack.getTexture("EmeraldPrincess_weapon_1");
		t_weapon_2 = GV.tPack.getTexture("EmeraldPrincess_weapon_2");
		
		super(t_noweapon_1);
		
		System.keyboard.down.connect(keyboardHandler);
		System.pointer.down.connect(pointerHandler);
		pointerDown.connect(pointerDownHandler);
		canShoot = true;
		this.anchorX._ = GV.k / 2;
	}
	
	public function giveGun(bool:Bool) {
		Player.hasGun = bool;
		System.storage.set(GV.keyHasGun, bool);
		canShoot = true;
	}
	
	function pointerHandler(event:PointerEvent) 
	{
		if (scene.getHud().isInPause())
			return;
		//  trace("clic X " + (event.viewX - GV.x) + " (" + event.viewX + " " + GV.x +") vs " + x._);

		var pointerX = (event.viewX - GV.x) /*/ GV.scale*/;
		pointerX -= scene.orig.x;
		
		//move left 
		if (pointerX < x._)	moveLeft();
		//move right
		else if (pointerX > x._) moveRight();
	}
	
	function pointerDownHandler(event:PointerEvent) 
	{
		event.stopPropagation();
		shoot();
	}

	function keyboardHandler(event:KeyboardEvent) {
		if (scene.getHud().isInPause())
			return;

		if (event.key == Key.Left)
			moveLeft();
			
		if (event.key == Key.Right)
			moveRight();
		
		if (event.key == Key.Space) {
			var s = shoot();
		}
		
		/* TODO : slomo while pushing a key
		if (event.key == Key.C) {
			if (isInSlowMode == false) {
				isInSlowMode = true;
				trace("sloooooooowmode" + isInSlowMode);
				scene.playSlowly();
			}
			if ( isInSlowMode == true) {
				isInSlowMode = false;
				trace("sloooooooowmode" + isInSlowMode);
				scene.play();
			}
			
		}*/
	}
	
	function shoot() {
		if (scene.isActive() && canShoot == true  && scene.getHud().getBulletLeft() > 0) {
			canShoot = false;
			var x:Int = Std.int(x._ / GV.k);
			var y:Int = Std.int(y._ / GV.k);
			GV.bPack.getSound("shot").play();
			scene.addBullet(x, y);
			scene.getHud().incrementShots(1);
			return true;
		}
		return false;
	}
	
	function moveLeft() {
		if( x._ - anchorX._ > 0 ) {
			var xTile:Int = Std.int(x._ / GV.k);
			x.animateTo((xTile-1) * GV.k+GV.k/2, 0.2, Ease.quadOut);
			scaleX.animateTo( -1, 0.2);
			//trace("xpos :"+x._);
		}
	}
	
	function moveRight() {
		if( x._ < scene.mapWidthInPx() - getNaturalWidth()*scaleX._) {
			var xTile:Int = Std.int(x._ / GV.k);
			x.animateTo((xTile + 1) * GV.k+GV.k/2, 0.2, Ease.quadOut);
			scaleX.animateTo(1, 0.2);
			//trace("xpos :"+x._);
		}
	}
	
	override public function onUpdate (dt :Float)
    {
		super.onUpdate(dt);
		animTimer -= dt;
		if (animTimer <= 0) {
			animTimer = 1;
			
			if(Player.hasGun == false) {
				if(texture == t_noweapon_1) texture = t_noweapon_2;
				else texture = t_noweapon_1;
			}
			
			else if(Player.hasGun == true) {
				if(texture == t_weapon_1) texture = t_weapon_2;
				else texture = t_weapon_1;
			}
		}
 	}
	
	public function collideWith(targetXTile:Int, targetYTile:Int) :Bool
	{
		var xTile:Int = Std.int(x._ / GV.k);
		var yTile:Int = Std.int(y._ / GV.k);
		//trace(xTile, yTile + " vs " + targetXTile, targetYTile);
		
		if (targetXTile == xTile && targetYTile == yTile)
			return true;
			
		if (targetXTile == xTile && targetYTile-1 == yTile)
			return true;
		
		return false;
	}
	
	public function resetShoot(){
		this.canShoot = true;
	}
}