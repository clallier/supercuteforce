package supercuteforce;
import flambe.Component;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.sound.Playback;
import flambe.sound.Sound;
import flambe.System;


/**
 * ...
 * @author Godjam - QilinEggs
 */
class MusicPlayer extends Component
{	
	var isInited:Bool = false;
	//stores
	var script:Script;
	var samples:Array<String>;
	var sequences:Array<Array<Int>>;
	var maxLoops = 3;
	var sampleLength:Float = 0;
	
	// current
	var power:Int;
	var loopCount:Int;
	var playbacks:Array<Playback> = null;
	var sequencePlaying:Int;	// sequence playing 
	var sequenceNext:Int;		// sequence next to play
	
	public function new() 
	{
		script = new Script();
		
		samples = new Array();
		samples.push("M0");
		samples.push("M1");
		samples.push("M2");
		samples.push("M3");
		samples.push("M4");
		samples.push("M5");
		samples.push("M6");
		samples.push("M7");
		
		sequences = new Array<Array<Int>>();
		sequences.push([0]);
		sequences.push([1]);
		sequences.push([2]);
		sequences.push([3]);
		sequences.push([4]);
		sequences.push([5]);
		sequences.push([6]);
		sequences.push([7]);
		
		playbacks = new Array<Playback>();
		
		sequencePlaying = -1;
		sequenceNext = -1;
		power = 0;
		loopCount = 0;
	}
	
	override public function onAdded() {
		if (isInited == false && owner != null) {
			owner.addChild(new Entity().add(script));
			script.run(new Sequence([new Delay(5), new CallFunction(startMusic)]));
			isInited = true;
		}
	}
	
	override public function onRemoved() {
		super.onRemoved();
	}
	
	public function startMusic() {
		setNextSeq(0);
		power = 0;
	}
	
	// invoked whenever the low-level sound API requires more sample data
	override public function onUpdate(dt:Float) {
		super.onUpdate(dt);
		if (GV.finishedLoadMusic == false) return ;
		
		if (playbacks[0] == null || playbacks[0].position >= (sampleLength-0.2/*.04*/)) {
			
			// we have something next to play
			if(sequenceNext != -1) {		
				sequencePlaying = sequenceNext;
				sequenceNext = -1;
				loopCount = 0;
			}
			
			
			// else just loop the current sample
			
			/// we read the previous loop 4 times => change loop
			if (loopCount > maxLoops && power == 0) {
				sequencePlaying = 1;
				loopCount = 0;
				power = 1;
			}
			// if power > 0 => decrease current power 
			if (loopCount >= maxLoops && power > 0) {
				loopCount = 0;
				power--;
				sequencePlaying = power;
			}
				
			//trace("looping " + sequencePlaying); 
			
			// clean all 
			playbacks = new Array<Playback>();

			if(sequences!= null && sequences[sequencePlaying] != null)
			for (i in sequences[sequencePlaying]) {
				loopCount++;
				var snd:Sound = GV.mPack.getSound(samples[i]);
				playbacks.push(snd.play());
				sampleLength = snd.duration;
				//trace("add sample " + i + " duration" + sampleLength);
			}
			
		}
	}
	
	// param 'name' is the name of the zone or fill
	public function setNextSeq(seq:Int) {
		if (seq >= sequences.length) return;		
		sequenceNext = seq;
		trace("next seq : will play " + seq); 
	}
	
	
	// param 'name' is the name of the zone or fill
	public function powerUp() {
		power++;
		if (power >= sequences.length) {
			power = sequences.length -1;
		}
		sequenceNext = power;
		trace("powerup : will play " + sequenceNext);
	}
	
	override public function dispose() 
	{
		super.dispose();
		cleanPlaybacks();
	}
	
	function cleanPlaybacks():Void 
	{
		if(playbacks != null) 
			for (playback in playbacks)	
			if (playback != null)  {
					playback.volume.animateTo(0, 0.5);
				}
			
		playbacks = null;
	}
	
}