package supercuteforce.gui;

import flambe.display.Sprite;
import flambe.display.PatternSprite;
import flambe.display.ImageSprite;
import flambe.display.Texture;
import flambe.Entity;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class ThreePatch extends Sprite
{
	var center:PatternSprite;
	var left:ImageSprite;
	var right:ImageSprite;
	var width:Int = 0;
	var height:Int = 0;
	
	public function new(leftTexture:String, centerTexture:String, rightTexture:String, width:Int)
	{
		super();
		left = new ImageSprite(GV.tPack.getTexture(leftTexture));
		right = new ImageSprite(GV.tPack.getTexture(rightTexture));
		
		var centerWidth:Int = width - Std.int(left.getNaturalWidth() + right.getNaturalWidth());
		center = new PatternSprite(GV.tPack.getTexture(centerTexture), centerWidth);
		
		left.setXY(0, 0);
		center.setXY(Std.int(left.getNaturalWidth()), 0);
		right.setXY(Std.int(center.getNaturalWidth() + left.getNaturalWidth()), 0);
		
		this.width = width;
		this.height = Std.int(center.getNaturalHeight());
	}
	
	override public function onAdded()
	{
		owner.addChild(new Entity().add(left));
		owner.addChild(new Entity().add(center));
		owner.addChild(new Entity().add(right));
	}
	
	override public function centerAnchor():Sprite {
		var w = width;
		var h = height;
		return super.centerAnchor();
	}
	
	override public function getNaturalWidth() :Float
	{
		return width;
	}
	
	override public function getNaturalHeight() :Float
	{
			return height;
	}
}