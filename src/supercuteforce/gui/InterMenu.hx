package supercuteforce.gui;
import flambe.display.FillSprite;
import flambe.display.Font.TextAlign;
import flambe.display.Sprite;
import flambe.display.TextSprite;
import flambe.Entity;
import flambe.scene.Scene;
import flambe.script.AnimateTo;
import flambe.script.CallFunction;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.scene.GameScene;
import flambe.scene.Director;
import flambe.scene.FadeTransition;
import flambe.animation.Ease;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class InterMenu extends Scene
{
	var textSprite:TextSprite;
	var menu:VerticalMenu;
	var bg:NinePatch;
	var scene:GameScene;
	var width:Int = 0;
	var height:Int = 0;
	var padding:Int = 6;
	var script:Script;
	var mainSprite:Sprite;
	
	public function new(scene:GameScene, headline:String) 
	{
		super(false);
		script = new Script();
		this.scene = scene;
		padding = 6;
		
		textSprite = new TextSprite(GV.font, headline);
		textSprite.align = TextAlign.Center;
		
		menu = new VerticalMenu();
	}
	
	override public function onAdded() {
		
		var p = 2 * padding; 
		width = Std.int(Math.max(textSprite.getNaturalWidth(), menu.getNaturalWidth())) + p;
		
		textSprite.setXY(width/2, padding);
		
		menu.setXY(width/2, textSprite.getNaturalHeight() +2*p);
		
		
		height = Std.int(textSprite.getNaturalHeight() + menu.getNaturalHeight()) + p;
		bg = new NinePatch("talk_tl", "talk_tc", "talk_tr", "talk_ml", "talk_mc", "talk_mr", "talk_bl", "talk_bc", "talk_br", 
			width, height);
		
		mainSprite = new FillSprite(0xffffff, width, height);
			
		owner.add(script);
		owner.add(mainSprite);
		owner.addChild(new Entity().add(bg));
		owner.addChild(new Entity().add(textSprite));
		owner.addChild(new Entity().add(menu));
		
		trace("adding the intermenu content : "+width+", "+height);
		mainSprite.centerAnchor();
		mainSprite.setXY(GV.width / 2, -GV.height / 2);
		
		// anim stuff
		mainSprite.scaleX.animate(0.2, 1.0, 0.5, Ease.sineInOut);
		mainSprite.scaleY.animate(0.2, 1.0, 0.5, Ease.bounceOut);
		mainSprite.y.animateTo(GV.height / 2, 0.5, Ease.bounceOut);
		mainSprite.alpha.animate(0, 0.9, 0.5, Ease.sineOut);
	}
	
	function delete() 
	{		
		var d:Director = GV.getRoot().get(Director);
		if (d != null) {
			d.popScene(new FadeTransition(1));
		}
	}
}