package supercuteforce.gui;
import flambe.animation.AnimatedFloat;
import flambe.display.FillSprite;
import flambe.display.PatternSprite;
import flambe.display.Sprite;
import flambe.Entity;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class ProgressBar extends Sprite
{
	var bar:PatternSprite;
	var maxVal:Float;
	var maxWidth:Float;
	
	public function new(width:Float, maxVal:Float=1) 
	{
		// super
		super();
		
		//bar = new FillSprite(0x00D3D3, width, GV.k / 2);
		bar = new PatternSprite(GV.tPack.getTexture("bbl_mc"), width, GV.k / 2);
		bar.alpha._ = 1;
		
		this.maxVal = maxVal;
		this.maxWidth = width;
	}
	
	override public function onAdded()
	{
		super.onAdded();
		owner.addChild(new Entity().add(bar));
	}
	
	public function onProgress(oldVal:Float, newVal:Float) {
		var ratio = newVal / maxVal;
		bar.width._ = ratio * maxWidth;
		bar.alpha._ = ratio;
	}
}