package supercuteforce.gui;

import flambe.display.Sprite;
import flambe.Entity;
import flambe.input.KeyboardEvent;
import flambe.input.PointerEvent;
import flambe.input.Key;
import flambe.System;
import supercuteforce.scene.GameScene;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Dialog extends Sprite
{
	var characters:Array<String>;
	var texts:Array<String>;
	var positions:Array<Bool>;
	var current:Int;
	var talk:TalkBubble;
	var callBack:Void->Void;
	var initialized:Bool;
	
	public function new() 
	{
		super(); 
		characters = new Array<String>();
		texts = new Array<String>();
		positions = new Array<Bool>();
		current = -1;
		//trace("set current to " + current);
		
		pointerDown.connect(mouseNextEvent);
		System.keyboard.down.connect(keyboardNextEvent);
		initialized = false;
	}
	
	public function addTalk(character:String, title:String, text:String, position:Bool) {
		characters.push(character);
		texts.push("["+title+"]\n"+text);
		positions.push(position);
	}
	
	public function nextTalk() {
		
		// show all if not complete
		if (talk != null && talk.isComplete() == false) talk.showAllText();
		
		// dispose if complete
		else {
			
			if (talk != null) {
				talk.owner.dispose();
				talk = null;
				//trace("disposing " + current);
			}
			
			++ current;
			
			if (current == 0) initialized = true;
		
			if (current < characters.length) {		
				//trace("loading " + current);
				talk = new TalkBubble(characters[current], texts[current], positions[current]);	
			
				if(owner != null)
					owner.addChild(new Entity().add(talk));
			} 
			
			else if(current == characters.length) {
				if(callBack != null) callBack();
				owner.dispose();
				//trace("disposing the dialog");
			}
		}
	}
	
	public function executeAtEnd(fn:Void->Void) 
	{
		callBack = fn;
	}
	
	function mouseNextEvent(event:PointerEvent) {
		event.stopPropagation();
		nextTalk();
	}
	
	function keyboardNextEvent(event:KeyboardEvent) 
	{
		if (event.key == Key.Space && initialized) {
			nextTalk();
		}
	}
	
}