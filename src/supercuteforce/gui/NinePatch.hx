package supercuteforce.gui;

import flambe.display.Sprite;
import flambe.display.PatternSprite;
import flambe.display.ImageSprite;
import flambe.Entity;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class NinePatch extends Sprite
{
	var topLeft:Sprite;
	var topCenter:Sprite;
	var topRight:Sprite;
	var midLeft:Sprite;
	var midCenter:Sprite;
	var midRight:Sprite;
	var downLeft:Sprite;
	var downCenter:Sprite;
	var downRight:Sprite;
	
	/**
	 * Big Ctor !
	 * @param	topLeft
	 * @param	topCenter
	 * @param	topRight
	 * @param	midLeft
	 * @param	midCenter
	 * @param	midRight
	 * @param	downLeft
	 * @param	downCenter
	 * @param	downRight
	 * @param	width
	 * @param	height
	 */
	public function new(topLeft:String, topCenter:String, topRight:String,
						midLeft:String, midCenter:String, midRight:String,
						downLeft:String, downCenter:String, downRight:String,
						width:Int, height:Int) 
	{
		super();
		var centerWidth:Int = 0, centerHeight:Int = 0, centerColumnLeft:Int = 0, rightColumnLeft:Int = 0; 
		var midLineTop:Int = 0, downLineTop:Int = 0;
		
		//top row 
		this.topLeft = new ImageSprite(GV.tPack.getTexture(topLeft));
		this.topRight = new ImageSprite(GV.tPack.getTexture(topRight));
		centerWidth = Std.int(width - this.topLeft.getNaturalWidth() - this.topRight.getNaturalWidth());
		this.topCenter = new PatternSprite(GV.tPack.getTexture(topCenter), centerWidth);
		
		// down row
		this.downLeft = new ImageSprite(GV.tPack.getTexture(downLeft));
		this.downRight = new ImageSprite(GV.tPack.getTexture(downRight));
		this.downCenter = new PatternSprite(GV.tPack.getTexture(downCenter), centerWidth);

		// mid row
		centerHeight =  height - Std.int(this.topLeft.getNaturalHeight() - this.downLeft.getNaturalHeight());
		this.midLeft = new PatternSprite(GV.tPack.getTexture(midLeft), -1, centerHeight);
		this.midRight = new PatternSprite(GV.tPack.getTexture(midRight), -1, centerHeight);
		this.midCenter = new PatternSprite(GV.tPack.getTexture(midCenter), centerWidth, centerHeight);
		
		//top row pos 
		centerColumnLeft =  Std.int(this.topLeft.getNaturalWidth());
		rightColumnLeft = centerWidth +centerColumnLeft;
		this.topLeft.setXY(0, 0);
		this.topRight.setXY(rightColumnLeft, 0);
		this.topCenter.setXY(centerColumnLeft, 0);
		
		// mid row position
		midLineTop =  Std.int(this.topLeft.getNaturalHeight());		
		this.midLeft.setXY(0, midLineTop);
		this.midRight.setXY(rightColumnLeft, midLineTop);
		this.midCenter.setXY(centerColumnLeft, midLineTop);
		
		//down row pos 
		downLineTop = midLineTop + centerHeight;
		this.downLeft.setXY(0, downLineTop);
		this.downRight.setXY(rightColumnLeft, downLineTop);
		this.downCenter.setXY(centerColumnLeft, downLineTop);
	}
	
	override public function onAdded()
	{
		owner.addChild(new Entity().add(topLeft));
		owner.addChild(new Entity().add(topCenter));
		owner.addChild(new Entity().add(topRight));
		
		owner.addChild(new Entity().add(midLeft));
		owner.addChild(new Entity().add(midCenter));
		owner.addChild(new Entity().add(midRight));
		
		owner.addChild(new Entity().add(downLeft));
		owner.addChild(new Entity().add(downCenter));
		owner.addChild(new Entity().add(downRight));
	}
}