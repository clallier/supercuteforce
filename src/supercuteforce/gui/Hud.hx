package supercuteforce.gui;

import flambe.Component;
import flambe.display.ImageSprite;
import flambe.display.Sprite;
import flambe.display.Texture;
import flambe.Disposer;
import flambe.Entity;
import flambe.display.TextSprite;
import flambe.debug.FpsDisplay;
import flambe.System;
import flambe.display.Font;
import flambe.display.PatternSprite;
import flambe.input.KeyboardEvent;
import flambe.input.PointerEvent;
import flambe.input.Key;
import supercuteforce.scene.GameScene;


/**
 * ...
 * @author Godjam - QilinEggs
 */
class Hud extends Sprite
{
	static var totalScore:Int = 0;
	var scene:GameScene;
	var level:String;
	var score:Int = 0;
	var shots:Int = 0;
	var bulletsLeft = 1;
	var isPuzzleMode = false;
	var text:TextSprite;
	var bg:PatternSprite;
	var pause:ImageSprite;
	var disposer:Disposer;
	var inPause:Bool;
	
	public function new(scene:GameScene, level:String) 
	{
		super();
		this.scene = scene;
		pause = new ImageSprite(GV.tPack.getTexture("pause_inactive"));
        text = new TextSprite(GV.dlgfont, 	GV.getText(328) + "  " +
											GV.getText(329) + "      " +
											GV.getText(330) + "  ");
		this.level = level;
		inPause = false;
		
		var barWidth = 2 * GV.k;
		var totalWidth = text.getNaturalWidth() + barWidth;
		
		bg = new PatternSprite(GV.tPack.getTexture("bar_bg"), GV.width);
		visible = false;
		
		text.setXY(GV.k*2, Std.int(-GV.k/3));
		
		// connection
		disposer = new Disposer();
		disposer.connect1(pause.pointerDown, handlePointer);
		disposer.connect1(System.keyboard.down, handleKeys);
		disposer.connect0(System.keyboard.backButton, activatePause);
		disposer.connect2(System.hidden.changed, handleHidden);
	}
	
	override public function onAdded() 
	{
		super.onAdded();
		owner.addChild(new Entity().add(bg));
		owner.addChild(new Entity().add(pause));
		owner.addChild(new Entity().add(text));
		writeText();
	}
	
	public function incrementScore(incr:Int) 
	{
		score += incr;
		writeText();
	}
	
	public function incrementShots(incr:Int) 
	{
		shots += incr;
		if(isPuzzleMode) --bulletsLeft;
		
		writeText();	
	}
	
	public function getScore() 
	{
		return score;
	}
	
	public function getTotalScore() 
	{
		return totalScore;
	}
	
	public function validateScore() {
		totalScore += score;
	}
	
	public function getShots() 
	{
		return shots;
	}
	
	public function getBulletLeft() 
	{
		return bulletsLeft;
	}
	
	public function setBulletLeft(bullets:Int) 
	{
		isPuzzleMode = true;
		bulletsLeft = bullets;
		writeText();
	}
	
	function writeText() {
		
		if (isPuzzleMode)
			text.text = GV.getText(328) + level + 
						GV.getText(329) + score + 
						GV.getText(330) + bulletsLeft;
		// if we just count shots
		else 
			text.text = GV.getText(328) + level +
						GV.getText(329) + score +
						GV.getText(331) + shots;
	}
	
	function handleKeys(event:KeyboardEvent) {
		if (event.key == Key.Escape) {
			activatePause();
		}
	}
	
	function handlePointer(event:PointerEvent) 
	{
		event.stopPropagation();
		activatePause();
	}
	
	function handleHidden(oVal:Bool, nVal:Bool) 
	{
		if (oVal == false && nVal == true) {
			activatePause();	
		}
	}
	
	public function desactivatePause() 
	{
		inPause = false;
	}
	
	public function isInPause() 
	{
		return inPause;
	}
	
	function activatePause():Void 
	{
		if (scene.isActive() && inPause == false) {
			inPause = true;
			scene.gotoPauseMenu();
		}
	}
}