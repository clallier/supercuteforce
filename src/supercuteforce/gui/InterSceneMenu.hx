package supercuteforce.gui;
import supercuteforce.scene.GameScene;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class InterSceneMenu extends InterMenu
{

	
	public function new(scene:GameScene, victory:Bool, shuffleMode:Bool = false) 
	{
		var text:String = "";
		
		// victory
		if (victory) {
			text += GV.getText(322);
			GV.bPack.getSound("win").play();
		}
		
		//lose 
		else {
			text += GV.getText(323);
			GV.bPack.getSound("lose").play();
		}
		
		// score
		text += "\n" + GV.getText(324)+ scene.getHud().getScore() 
			+ "\n" + GV.getText(325)+ (scene.getHud().getTotalScore() + scene.getHud().getScore());
			
		super(scene, text);
		
		// retry
		menu.addButton(new Button("btn_retry", "btn_center", "btn_right", GV.getText(317), gotoRetry));
		
		if (victory && shuffleMode == false) {
			// next
			menu.addButton(new Button("btn_next", "btn_center", "btn_right", GV.getText(326), gotoNext), true);
		} else {
			// fast retry
			menu.addButton(new Button("btn_retry", "btn_center", "btn_right", GV.getText(318), gotoFastRetry), true);
		}
		
		if (shuffleMode == false) {
			// Levels
			menu.addButton(new Button("btn_hud", "btn_center", "btn_right", GV.getText(319), gotoLevels));
		} else {
			// Suffle Menu
			menu.addButton(new Button("hud_prev", "btn_center", "btn_right", GV.getText(320), gotoShuffle));
		}
	}
	
	override public function onUpdate(dt:Float)
	{
		super.onUpdate(dt);
		GameScene.musicPlayer.onUpdate(dt);
	}
	
	function gotoLevels() 
	{
		delete();
		scene.gotoHub();
	}
	
	function gotoNext() 
	{
		delete();
		scene.gotoNextScene();
	}
	
	function gotoRetry() 
	{
		delete();
		scene.gotoRetry();
	}
	
	function gotoFastRetry() 
	{
		delete();
		scene.gotoFastRetry();
	}
	
	function gotoShuffle() 
	{
		delete();
		scene.gotoShuffleMenu();
	}
}