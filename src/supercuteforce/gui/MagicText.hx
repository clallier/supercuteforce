package supercuteforce.gui;

import flambe.display.ImageSprite;
import flambe.display.Sprite;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Repeat;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.util.Signal0.Listener0;
import flambe.display.TextSprite;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class MagicText extends Button
{
	var icons:Array<ImageSprite>;
	var texts:Array<TextSprite>;
	var idx = 0;
	//var timer:Script;
	var timer:Float = 0;
	var padleft:Int;
	
	public function new(target:Listener0=null) 
	{
		icons = new Array<ImageSprite>();
		icons.push(new ImageSprite(GV.tPack.getTexture("godjam")));
		icons.push(new ImageSprite(GV.tPack.getTexture("qilin")));
		icons.push(new ImageSprite(GV.tPack.getTexture("flambe")));
		icons.push(new ImageSprite(GV.tPack.getTexture("music")));
		texts = new Array<TextSprite>();
		texts.push(new TextSprite(GV.font, GV.getText(345)));
		texts.push(new TextSprite(GV.font, GV.getText(346)));
		texts.push(new TextSprite(GV.font, GV.getText(347)));
		texts.push(new TextSprite(GV.font, GV.getText(348)));
		
		super("btn_left", "btn_center", "btn_right", GV.getText(345), target);
	}
	
	override public function onAdded()
	{
		super.onAdded();
		text.visible = false;
		for(i in 0...texts.length) {
			owner.addChild(new Entity().add(texts[i]));
			texts[i].centerAnchor();
			texts[i].setXY(textOffset, 0);
		}
		
		for(i in 0...icons.length){
			owner.addChild(new Entity().add(icons[i]));
			var textWidth = texts[i].getNaturalWidth() / 2;
			var iconWidth = icons[i].getNaturalWidth();
			var iconHeight = icons[i].getNaturalHeight();
			icons[i].setAnchor(-textOffset/2, iconHeight/2);
			icons[i].setXY(-width/2, 0);
		}
		
		//timer.run(new Repeat(new Sequence([new CallFunction(changeText), new Delay(2)])));
	}
	
	function changeText() {
		for(i in 0...icons.length) {
			if (i == idx) icons[i].alpha.animateTo(1, 0.2);
			else icons[i].alpha.animateTo(0, 0.2);
		}
		
		for(i in 0...texts.length) {
			if (i == idx) texts[i].alpha.animateTo(1, 0.2);
			else texts[i].alpha.animateTo(0, 0.2);
		}
		
		++idx;
		if (idx >= texts.length) idx = 0;
	}
	
	override public function onUpdate(dt:Float) {
		super.onUpdate(dt);
		
		timer -= dt;
		
		if (timer <= 0) {
			timer = 2;
			changeText();
		}
		
	}
	
	
}