package supercuteforce.gui;
import flambe.animation.Ease;
import flambe.scene.FadeTransition;
import flambe.scene.SlideTransition;
import flambe.System;
import supercuteforce.scene.HowToScene;
import supercuteforce.scene.GameScene;
import supercuteforce.scene.HubScene1;
import supercuteforce.scene.HubScene2;
import supercuteforce.scene.HubScene3;
import supercuteforce.scene.ShuffleSelectionScene;
import supercuteforce.scene.IntroScene;
import supercuteforce.scene.Level1;
import supercuteforce.scene.Level10;
import supercuteforce.scene.Level11;
import supercuteforce.scene.Level12;
import supercuteforce.scene.Level13;
import supercuteforce.scene.Level14;
import supercuteforce.scene.Level15;
import supercuteforce.scene.Level16;
import supercuteforce.scene.Level17;
import supercuteforce.scene.Level18;
import supercuteforce.scene.Level19;
import supercuteforce.scene.Level2;
import supercuteforce.scene.Level20;
import supercuteforce.scene.Level21;
import supercuteforce.scene.Level22;
import supercuteforce.scene.Level23;
import supercuteforce.scene.Level24;
import supercuteforce.scene.Level25;
import supercuteforce.scene.Level26;
import supercuteforce.scene.Level27;
import supercuteforce.scene.Level3;
import supercuteforce.scene.Level4;
import supercuteforce.scene.Level5;
import supercuteforce.scene.Level6;
import supercuteforce.scene.Level7;
import supercuteforce.scene.Level8;
import supercuteforce.scene.Level9;
import supercuteforce.scene.ShuffleEasyGameScene;
import supercuteforce.scene.ShuffleMediumGameScene;
import supercuteforce.scene.ShuffleHardGameScene;
import supercuteforce.scene.ShuffleVHardGameScene;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Scenario
{
	var currentScene:Int;
	
	public static function createScene(idx:Int, reset:Bool = false) : GameScene {
		if (idx == 1) return new Level1(reset);
		if (idx == 2) return new Level2(reset);
		if (idx == 3) return new Level3(reset);
		if (idx == 4) return new Level4(reset);
		if (idx == 5) return new Level5(reset);
		if (idx == 6) return new Level6(reset);
		if (idx == 7) return new Level7(reset);
		if (idx == 8) return new Level8(reset);
		if (idx == 9) return new Level9(reset);
		if (idx == 10) return new Level10(reset);
		if (idx == 11) return new Level11(reset);
		if (idx == 12) return new Level12(reset);
		if (idx == 13) return new Level13(reset);
		if (idx == 14) return new Level14(reset);
		if (idx == 15) return new Level15(reset);
		if (idx == 16) return new Level16(reset);
		if (idx == 17) return new Level17(reset);
		if (idx == 18) return new Level18(reset);
		if (idx == 19) return new Level19(reset);
		if (idx == 20) return new Level20(reset);
		if (idx == 21) return new Level21(reset);
		if (idx == 22) return new Level22(reset);
		if (idx == 23) return new Level23(reset);
		if (idx == 24) return new Level24(reset);
		if (idx == 25) return new Level25(reset);
		if (idx == 26) return new Level26(reset);
		if (idx == 27) return new Level27(reset);
		if (idx == 28) return new ShuffleEasyGameScene(reset);
		if (idx == 29) return new ShuffleMediumGameScene(reset);
		if (idx == 30) return new ShuffleHardGameScene(reset);
		if (idx == 31) return new ShuffleVHardGameScene(reset);
		
		else return new IntroScene();
	}

	public function new() {
		currentScene = System.storage.get(GV.keyLevel, 1);
	}

	public function getFirstScene() : GameScene
	{
		currentScene = System.storage.get(GV.keyLevel, 1);
		if(currentScene == 1)
			return getScene(currentScene);
		else 
			return getHub(currentScene);
	}
	
	private function getScene(i:Int, noDlg:Bool = false) : GameScene
	{
		currentScene = i;
		var scene = createScene(i, noDlg);
		scene.setScenario(this);
		return scene;
	}
	
	function getHub(i:Int):GameScene 
	{
		var scene:GameScene;
		
		if (i> 18)
			scene = new HubScene3();
		else if (i> 9)
			scene = new HubScene2();
		else
			scene = new HubScene1();
		
		scene.setScenario(this);
		return scene;
	}
	
	public function gotoScene(i:Int) 
	{
		var transition = new FadeTransition(1, Ease.bounceInOut);
		GV.unwindToScene(getScene(i).getScene(), transition);
	}
	
	public function gotoNextScene() 
	{
		++currentScene;
		var transition = new SlideTransition(1, Ease.cubeInOut).up();
		GV.unwindToScene(getScene(currentScene).getScene(), transition);
	}
	
	public function gotoHub() 
	{
		var scene = getHub(currentScene);
		var transition = new FadeTransition(1, Ease.bounceInOut);
		GV.unwindToScene(scene.getScene(), transition);
	}
	
	public function gotoShuffleMenu() 
	{
		var scene = new ShuffleSelectionScene();
		scene.setScenario(this);
		var transition = new SlideTransition(1, Ease.cubeOut).down(); 
		GV.unwindToScene(scene.getScene(), transition);
	}
	
	public function gotoFirstScene() 
	{
		var transition = new SlideTransition(1, Ease.cubeOut).down(); 
		GV.unwindToScene(getFirstScene().getScene(), transition);
	}
	
	public function gotoMainMenu() 
	{
		var scene = new IntroScene();
		scene.setScenario(this);
		GV.unwindToScene(scene.getScene(), new FadeTransition(0.5));
		
		GV.startLoadingMusicPack();
		GV.resize();
	}
	
	public function gotoRetry() 
	{
		var transition = new FadeTransition(1, Ease.cubeInOut);
		GV.unwindToScene(getScene(currentScene).getScene(), transition);
	}
	
	public function gotoFastRetry() 
	{
		var transition = new FadeTransition(1, Ease.cubeInOut);
		GV.unwindToScene(getScene(currentScene, true).getScene(), transition);
	}
	
	public function validateWinLevel():Void 
	{
		var access = System.storage.get(GV.keyLevel, 1);
		
		// saving acces to next level
		if(access < currentScene+1)
			System.storage.set(GV.keyLevel, currentScene+1);
	}
	
	public function gotoInfinityModeEasy() 
	{
		var transition = new FadeTransition(1, Ease.cubeInOut);
		GV.unwindToScene(getScene(28).getScene(), transition);
	}
	
	public function gotoInfinityModeMedium() 
	{
		var transition = new FadeTransition(1, Ease.cubeInOut);
		GV.unwindToScene(getScene(29).getScene(), transition);
	}
	
	public function gotoInfinityModeHard() 
	{
		var transition = new FadeTransition(1, Ease.cubeInOut);
		GV.unwindToScene(getScene(30).getScene(), transition);
	}
	
	public function gotoInfinityModeVHard() 
	{
		var transition = new FadeTransition(1, Ease.cubeInOut);
		GV.unwindToScene(getScene(31).getScene(), transition);
	}
	
	public function gotoHowToPlay() 
	{
		var scene:GameScene = new HowToScene();		
		scene.setScenario(this);
		
		var transition = new SlideTransition(1, Ease.bounceInOut).down();
		GV.unwindToScene(scene.getScene(), transition);
	}
}