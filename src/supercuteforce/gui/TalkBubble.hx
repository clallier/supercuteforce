package supercuteforce.gui;
import flambe.display.Font.TextLayout;
import flambe.display.Sprite;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Repeat;
import flambe.script.Sequence;
import supercuteforce.scene.GameScene;
import flambe.display.TextSprite;
import flambe.display.PatternSprite;
import flambe.display.ImageSprite;
import flambe.Entity;
import flambe.script.Script;
import flambe.display.Font.TextAlign;
import flambe.animation.Ease;

/**
 * ... 
 * @author Godjam - QilinEggs
 */
class TalkBubble extends Sprite
{
	// text 
	// portrait 
	// background
	var text:TextSprite;
	var background:NinePatch;
	var portrait:Sprite;
	var touchIcon:TouchIcon;
	var words:Array<String>;
	var i:Int;
	var targetY:Int;
	
	public function new(characterPortrait:String, txt:String, isTop:Bool) 
	{
		super();
		i = 0;
		var offset = GV.k /2;
		var maxTextWidth = (GV.mapWidth - 3) * GV.k - offset;
		var maxWidth = GV.mapWidth * GV.k;
		var align = TextAlign.Left;
		
		// text 
		txt += "\n";
		words = txt.split(""); 
 		text = new TextSprite(GV.dlgfont, txt);
		text.wrapWidth._ = maxTextWidth;
		text.align = align;
		var xPos = isTop ? offset : 2* offset + 2 * GV.k;
		text.setXY(xPos, offset);
		
		// touch icon
		touchIcon = new TouchIcon();
		touchIcon.setXY(xPos + text.getNaturalWidth() - touchIcon.getNaturalWidth()*2, 
						text.getNaturalHeight() - touchIcon.getNaturalHeight()*2);
		
		// portrait
		portrait = new ImageSprite(GV.tPack.getTexture(characterPortrait));
		var portraitWidth = GV.scaleTo(2 * GV.k, portrait.getNaturalWidth());
		portrait.setScale(portraitWidth);
		var xPos = isTop ? maxTextWidth + offset : offset;
		portrait.setXY(xPos, offset);
		
		// background
		var totalHeight = Std.int(Math.max(text.getNaturalHeight(), 3 * GV.k)+offset);
		background = new NinePatch("talk_tl", "talk_tc", "talk_tr", "talk_ml", "talk_mc", "talk_mr", "talk_bl", "talk_bc", "talk_br", 
			maxWidth, totalHeight);
		background.setXY(0, 0);
		background.alpha._ = 0.9;
			
		// position
		if(isTop) {
			setXY(0, -totalHeight);
			targetY = 0;
		} else { 
			setXY(0, GV.mapHeight * GV.k + totalHeight);
			targetY = GV.mapHeight * GV.k - totalHeight;
		}
		
		scaleX.animate(0.5, 1.0, 0.5, Ease.bounceOut);
		y.animateTo(targetY, 0.5, Ease.bounceOut);
		alpha.animate(0, 0.9, 0.5, Ease.sineOut);
	}

	override public function onAdded()
	{
		owner.addChild(new Entity().add(background));
		owner.addChild(new Entity().add(text));
		owner.addChild(new Entity().add(portrait));
		owner.addChild(new Entity().add(touchIcon));
		owner.add(new Script());
		
		// text functionality
		text.text = words[0];
		var s:Script = owner.get(Script);
		s.run(new Repeat(new Sequence([new Delay(0.05), new CallFunction(addWord)])));
	}
	
	function addWord() {
		
		if(i < words.length-1) {
			++i;
			text.text += "" + words[i];
			GV.bPack.getSound("talk").play();
		}
	}
	
	public function isComplete() :Bool 
	{
		return (i >= words.length - 1);
	}
	
	public function showAllText() {
		i = words.length;
		text.text = words.join("");
	}
}