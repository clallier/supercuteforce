package supercuteforce.gui;
import supercuteforce.scene.GameScene;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class ControlMenu extends InterMenu
{

	public function new(scene:GameScene, showHudBtn:Bool, showShuffleBtn:Bool, showRetry:Bool = true) 
	{
		super(scene, "Pause");
		menu.addButton(new Button("hud_next", "btn_center", "btn_right", GV.getText(316), gotoContinue), true);
		menu.addButton(new SoundButton("btn_left", "btn_center", "btn_right", GV.soundSwitch));
		
		if(showRetry) {
			menu.addButton(new Button("btn_retry", "btn_center", "btn_right", GV.getText(317), gotoRetry));
			menu.addButton(new Button("btn_retry", "btn_center", "btn_right", GV.getText(318), gotoFastRetry));
		}
		
		if(showHudBtn)
			menu.addButton(new Button("btn_hud", "btn_center", "btn_right", GV.getText(319), gotoLevels));
		
		if(showShuffleBtn) 
			menu.addButton(new Button("hud_prev", "btn_center", "btn_right", GV.getText(320), gotoShuffle));
		
		menu.addButton(new Button("btn_menu", "btn_center", "btn_right", GV.getText(321), gotoMenu));
	}
	
	function gotoContinue() 
	{
		delete();
		scene.play();
	}
	
	function gotoLevels() 
	{
		delete();
		scene.gotoHub();
	}
	
	function gotoMenu() 
	{
		delete();
		scene.gotoMainMenu();
	}
	
	function gotoRetry() 
	{
		delete();
		scene.gotoRetry();
	}
	
	function gotoFastRetry() 
	{
		delete();
		scene.gotoFastRetry();
	}
	
	function gotoShuffle() 
	{
		delete();
		scene.gotoShuffleMenu();
	}
}