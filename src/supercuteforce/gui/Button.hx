package supercuteforce.gui;

import flambe.animation.Ease;
import flambe.display.ImageSprite;
import flambe.display.Sprite;
import flambe.display.TextSprite;
import flambe.Disposer;
import flambe.Entity;
import flambe.input.Key;
import flambe.input.KeyboardEvent;
import flambe.input.PointerEvent;
import flambe.script.AnimateTo;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Parallel;
import flambe.script.Repeat;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.System;
import flambe.util.Signal0.Listener0;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Button extends Sprite
{
	var text:TextSprite;
	var background:ThreePatch;
	var back_hover:ThreePatch;
	var width:Int = 0;
	var height:Int = 0;
	var disposer:Disposer;
	var script:Script;
	public var target:Void->Void;
	var hasFocus:Bool;
	var isInit:Bool;
	var textOffset:Int;
	
	public function new(left:String, center:String, right:String, txt:String, target:Listener0=null) 
	{
		super();
		script = new Script();
		
		var leftSize = GV.tPack.getTexture(left).width;
		var offsetLeft = leftSize;
		var offsetRight = Std.int(GV.k/3);
		textOffset = (leftSize > GV.k/2)? Std.int(GV.k/3):0;
		
		
 		text = new TextSprite(GV.font, txt);
		text.centerAnchor();
		text.setXY(textOffset, 0);
		
		var bgWidth:Int = Std.int(text.getNaturalWidth() + offsetLeft + offsetRight);
		background = new ThreePatch(left, center, right, bgWidth);
		background.centerAnchor();
		
		buildhover(left, center, right, bgWidth);
		
		hasFocus = false;
		isInit = false;
		disposer = new Disposer();
		
		width = Std.int(Math.max(text.getNaturalWidth(), background.getNaturalWidth()));
		height = Std.int(Math.max(text.getNaturalHeight(), background.getNaturalHeight()));
		
		if (target != null) {
			pointerDown.connect(mouseHandler);
			disposer.add(System.keyboard.down.connect(keyboardHandler));
			this.target = target;
		}
	}
	
	function buildhover(left:String, center:String, right:String, bgWidth:Int) 
	{
		var left_hover = left + "_hover";
		var center_hover = center + "_hover";
		var right_hover = right + "_hover";
		back_hover = new ThreePatch(left_hover, center_hover, right_hover, bgWidth);
		back_hover.centerAnchor();
	}
	
	function mouseHandler(event:PointerEvent) 
	{
		if(isInit) {
			event.stopPropagation();
			executeTarget();
		}
	}
	
	public function isInited():Bool {
		return isInit;
	}
	
	public function keyboardHandler(event:KeyboardEvent) 
	{
		if (hasFocus == true && target != null && event.key == Key.Space && isInit == true) {
			//trace("btn " +text.text+" try to launch target, setFocus to "+hasFocus);
			hasFocus = false;
			executeTarget();
		}
	}
	
	public function activate() 
	{
		if (this.target == null || hasFocus) return;
		GV.bPack.getSound("activate").play();
		
		// change background if hover exists
		background.alpha.animateTo(0, 0.1);
		
		script.run(new Sequence([
		new Parallel([	new AnimateTo(back_hover.scaleX, 1.10, 0.1), 
						new AnimateTo(back_hover.scaleY, 1.0, 0.1)]),
		new Repeat(new Sequence([
			new Parallel([
				new AnimateTo(back_hover.scaleX, 1.0, 1, Ease.quadIn),
				new AnimateTo(back_hover.scaleY, 1.10, 1, Ease.quadIn)]),
				
			new Delay(0.2),
			new Parallel([
				new AnimateTo(back_hover.scaleX, 1.10, 1, Ease.quadIn),
				new AnimateTo(back_hover.scaleY, 1.0, 1, Ease.quadIn)]),
				]))
			]));
		hasFocus = true;
	}
	
	public function deactivate() 
	{
		script.stopAll(); //cleanup animation
		
		// change background if hover exists
		background.alpha.animateTo(1, 0.1);
		
		back_hover.scaleX.animateTo(1, 0.1, Ease.quadIn);
		back_hover.scaleY.animateTo(1, 0.1, Ease.quadIn);
		hasFocus = false;
	}
		
	override public function onAdded()
	{
		var s = new Script();

		if(owner != null) {
			owner.addChild(new Entity().add(disposer));
			owner.addChild(new Entity().add(back_hover));
			owner.addChild(new Entity().add(background));
			owner.addChild(new Entity().add(text));
			owner.addChild(new Entity().add(s));
			owner.add(script);
		}
		s.run(new Sequence([new Delay(1), new CallFunction(initBtn)]));
	}
	
	private function initBtn() {
		isInit = true;
	}
	
	override public function getNaturalWidth() :Float
	{
		return width;
	}
	
	override public function getNaturalHeight() :Float
	{
			return height;
	}
	
	function executeTarget() {
		GV.bPack.getSound("clic").play();
		disposer.dispose();
		script.run(new CallFunction(this.target));
	}
}