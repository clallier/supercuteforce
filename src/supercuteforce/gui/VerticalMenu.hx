package supercuteforce.gui;

import flambe.display.Sprite;
import flambe.Disposer;
import flambe.Entity;
import flambe.input.Key;
import flambe.input.KeyboardEvent;
import flambe.input.PointerEvent;
import flambe.System;


/**
 * ...
 * @author Godjam - QilinEggs
 */
class VerticalMenu extends Sprite
{
	var padding:Int;
	var currentY:Int;
	var buttons:Array<Button>;
	var activeBtn:Int;
	var width:Int = 0;
	var height:Int = 0;
	var disposer:Disposer;
	
	public function new() 
	{
		super();
		
		padding = 2;
		currentY = padding;
		activeBtn = 0;
		
		buttons = new Array<Button>();
		disposer = new Disposer();
		
		disposer.connect1(pointerMove, handlePointer);
		disposer.connect1(System.keyboard.down, handleKeys);
	}
	
	override function onAdded()
	{
		owner.addChild(new Entity().add(disposer));
		
		for(i in 0...buttons.length)
			owner.addChild(new Entity().add(buttons[i]));
		
		activate(activeBtn);
	}
	
	public function addButton(btn:Button, activateFirst:Bool = false) : VerticalMenu
	{
		if (btn == null) return this;
		btn.setXY(padding, currentY);
		
		buttons.push(btn);
		currentY += Std.int(btn.getNaturalHeight())+padding;
		
		// if we want to activate the first btn
		if (activateFirst)
			activeBtn = buttons.length -1;
			
		height = Std.int(currentY + btn.getNaturalHeight());
		
		if (btn.getNaturalWidth()> width)
			width = Std.int(btn.getNaturalWidth());
			
		return this;
	}
	
	function activate(idx:Int) {
		
		for (i in 0... buttons.length ) {
			var b:Button = buttons[i];
			if (b != null && i != idx)
				b.deactivate();
		} 
		
		var b:Button = buttons[idx];
		if (b != null) {
			b.dispose();
			owner.addChild(new Entity().add(b));
			b.activate();
			activeBtn = idx;
			//trace("btn :" + activeBtn + " is active"); 
		}
	}
	
	function handleKeys(event:KeyboardEvent) {
		if (event.key == Key.Down) {
			++activeBtn;
			if (activeBtn == buttons.length) activeBtn = 0;
			
			while(buttons[activeBtn].target == null) {
				++activeBtn;
				if (activeBtn == buttons.length) activeBtn = 0;
			}
			
		}
			
		if (event.key == Key.Up) {
			--activeBtn;
			if (activeBtn < 0) activeBtn = buttons.length - 1;
			
			while(buttons[activeBtn].target == null) {
				--activeBtn;
				if (activeBtn < 0) activeBtn = buttons.length-1;
			}
		}
		
		activate(activeBtn);
		if (event.key == Key.Space) {
			if(buttons[activeBtn].isInited()) {
				disposer.dispose();
				buttons[activeBtn].keyboardHandler(event);
			}
		}
	}
	
	function handlePointer(event:PointerEvent) 
	{
		var sprite:Sprite = event.hit.getParentSprite();
		
		if (sprite == null) return;
		
		for (i in 0... buttons.length ) {
			
			var b:Button = buttons[i];
			if (b == sprite) {
				activate(i);
				continue;
			}
		}
	}
	
	override public function getNaturalWidth() :Float
	{
		return width;
	}
	
	override public function getNaturalHeight() : Float
	{
		return height;
	}
	
	override public function setXY(x:Float, y:Float) :Sprite
	{
		var w = width;
		var h = height;
		return super.setXY(x, y);
	}
}