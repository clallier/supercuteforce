package supercuteforce.gui;
import flambe.display.Texture;
import flambe.display.ImageSprite;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Repeat;
import flambe.script.Script;
import flambe.script.Sequence;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class TouchIcon extends ImageSprite
{
	var textures:Array<Texture>;
	var time:Float = 1;
	var currentFrame = 0;
	
	public function new() 
	{
		textures = new Array<Texture>();
		textures.push(GV.tPack.getTexture("touch_icon_1"));
		textures.push(GV.tPack.getTexture("touch_icon_2"));
		currentFrame = 0;
		super(textures[currentFrame]);
	}
	
	override public function onAdded() 
	{
		owner.add(new Script());
		var s = owner.get(Script);
		
		if (s != null)
			s.run(new Repeat(new Sequence([new Delay(0.5), new CallFunction(execute)])));
	}
	
	function execute() {
		++currentFrame;
			
		if (currentFrame == textures.length)
			currentFrame = 0;
			
		texture = textures[currentFrame];
	}
}