package supercuteforce.gui;

import flambe.display.Sprite;
import flambe.display.Sprite;
import flambe.Entity;
import flambe.input.PointerEvent;
import flambe.subsystem.PointerSystem;
import flambe.util.SignalConnection;
import supercuteforce.scene.GameScene;

import flambe.input.Key;
import flambe.input.KeyboardEvent;
import flambe.System;
import flambe.Disposer;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class GridMenu extends VerticalMenu
{	
	var scene:GameScene;
	var currentX:Int;
	var prevBtn:Button;
	var nextBtn:Button;
	var mainBtn :Button;
	
	public function new(scene:GameScene) 
	{
		super();
		this.scene = scene;
		padding = GV.k;
		currentX = currentY = padding;
	}
	
	override function onAdded()
	{
		owner.addChild(new Entity().add(disposer));
		
		for(i in 0...buttons.length)
			owner.addChild(new Entity().add(buttons[i]));
		
		mainBtn = new Button("btn_menu", "btn_center", "btn_right", GV.getText(321), scene.gotoMainMenu);
		mainBtn.setXY(width / 2 -GV.k/2  , height + padding);
		owner.addChild(new Entity().add(mainBtn));
		height += padding + Std.int(mainBtn.getNaturalHeight());
		buttons.push(mainBtn);
		
		activate(activeBtn);
	}
	
	override public function addButton(btn:Button, activateFirst:Bool = false) : GridMenu
	{
		if (btn == null) return this;
		if (buttons.length > 9) {
			trace("GridMenu is full");
			return this; //nothing will be done
		}

		btn.setXY(currentX, currentY);		
		buttons.push(btn);
		
		currentX += Std.int(btn.getNaturalWidth()) + padding;
		
		if (buttons.length % 3 == 0) {
			
			if (currentX > width)
				width = currentX;
			
			currentX = padding;
			currentY += Std.int(btn.getNaturalHeight()) + padding;
			
			height = currentY;
		}
		
		
		// if we want to activate the first btn
		if (activateFirst)
			activeBtn = buttons.length -1;
			
		return this;
	}
	
	public function setPrevButton(btn:Button) : GridMenu
	{
		prevBtn = btn;
		prevBtn.setXY(padding, currentY);		
		buttons.push(prevBtn);
		
		return this;
	}
	
	public function setNextButton(btn:Button) : GridMenu
	{
		nextBtn = btn;
		nextBtn.setXY(width-btn.getNaturalWidth(), currentY);		
		buttons.push(nextBtn);
		
		return this;
	}
}