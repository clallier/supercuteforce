package supercuteforce.gui;
import flambe.System;
import flambe.util.Signal0.Listener0;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class SoundButton extends Button
{
	var snd_on:String; 
	var snd_off:String;
	
	public function new(left:String, center:String, right:String, target:Listener0=null) 
	{
		snd_on =  GV.getText(350);
		snd_off = GV.getText(351);
		
		if (System.volume._ == 0)
			super(left, center, right, snd_off, target);
		else super (left, center, right, snd_on, target);
	}
	
	override function executeTarget() {
		if (System.volume._ == 0) {
			updateWith(snd_on);
		} else {
			updateWith(snd_off);
		}
		super.executeTarget();
	}
	
	function updateWith(text:String) 
	{
		this.text.text = text;
	}
	
}