package supercuteforce;

import flambe.Component;
import flambe.display.ImageSprite;
import flambe.math.FMath;
import flambe.math.Point;
import flambe.System;
import supercuteforce.blocks.Block;
import supercuteforce.blocks.BlockFactory.BlockType;
import supercuteforce.scene.GameScene;


/**
 * ...
 * @author Godjam - QilinEggs
 */
class Bullet extends ImageSprite
{
	var type:Int = 0; // no type
	var aDegree:Float = 0;
	var aRad:Float = 0;
	var g:Int = 0;
	var scene:GameScene;

	public function new(scene:GameScene, x:Float, y:Float, angle:Float = 0, type:Int = 0, g:Int=1 ) {
		this.scene = scene;
		var textureName = "bullet_nocolor";
		if (type == BlockType.bubble_blue)
			textureName = "bullet_blue";
		else if (type == BlockType.bubble_green)
			textureName = "bullet_green";
		else if (type == BlockType.bubble_pink)
			textureName = "bullet_pink";
		else if (type == BlockType.bubble_red)
			textureName = "bullet_red";
		super(GV.tPack.getTexture(textureName));
		centerAnchor();
		setXY(x, y);
		aDegree = angle;
		aRad = FMath.toRadians(aDegree) + FMath.PI / 2;
		this.type = type;
		this.g = (g < 1024)?Math.floor(g*2):g;
	}
	
	override public function onUpdate(dt:Float)
	{
		var r = dt * 32*GV.k;
		if (r > GV.k) r = GV.k;
		y._ -= r * Math.sin(aRad);
		x._ += r * Math.cos(aRad);
		rotation._ = -aDegree;
		
		if (x._ < 0 || x._ > (scene.mapWidthInPx()) ||
            y._ < 0 || y._ > (scene.mapHeightInPx())) {
            scene.deleteMovingObject(this);
            return;
        }
		
		
		var tileX:Int = Math.floor(x._ / GV.k);
		var tileY:Int = Math.floor(y._ / GV.k);
		var b:Block = scene.getBlock(tileX, tileY);
		if (b != null) {
			if(type == 0 ||  (type == b.getEmittedType())) {
				b.explode(g);
				scene.incrementScore(g);
			}
			scene.deleteMovingObject(this);
		}
	}
}