package supercuteforce.blocks;

import flambe.display.Texture;
import supercuteforce.scene.GameScene;
import supercuteforce.blocks.BlockFactory.BlockType;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Rock extends Block
{

	public function new(scene:GameScene, x:Int, y:Int) 
	{
		super(GV.tPack.getTexture("rock"), scene, x, y);
		this.type = BlockType.rock;
	}
	
	override public function explode(g:Int)
	{
		// FTW!
		scene.shakeScreen(g);
	}
	
	override public function getEmittedType(): Int
	{
		return 0;
	}
}