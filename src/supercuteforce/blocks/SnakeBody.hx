package supercuteforce.blocks;
import supercuteforce.scene.GameScene;
import supercuteforce.blocks.BlockFactory.BlockType;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class SnakeBody extends Block
{
	public var xPrev(default, null) = -1;
	public var yPrev(default, null) = -1;
	public var xNext(default, null) = -1;
	public var yNext(default, null) = -1;
	
	
	public function new(scene:GameScene, x:Int, y:Int) 
	{
		super(GV.tPack.getTexture("snake_angle"), scene, x, y);
		this.type = BlockType.snake_body;
	}
	
	override public function copy(xTile:Int, yTile:Int) : Block
	{
		var dX = xTile - this.xTile; 
		var dY = yTile - this.yTile; 
		var b:SnakeBody = new SnakeBody(scene, xTile, yTile);
		xNext += dX;
		yNext += dY;
		if (xPrev != -1)	xPrev += dX;
		if (yPrev != -1) 	yPrev += dY;
		
		b.setPreviousPosition(xNext, yNext, xPrev, yPrev);
		return b;
	}
	
	public function setPreviousPosition(xOut:Int, yOut:Int, xIn:Int, yIn:Int) 
	{
		xPrev = xIn;
		yPrev = yIn;
		xNext = xOut;
		yNext = yOut;
	
		// vertical bar
		if (xIn == xOut){
			texture = GV.tPack.getTexture("snake_bar");
		}
		
		// horizontal bar 
		else if (yIn == yOut){
			texture = GV.tPack.getTexture("snake_bar");
			rotation._ = 90;
		}
		
		// tail
		else if (xIn == -1 && yIn == -1) {
			var dXOut = xTile - xOut;
			var dYOut = yTile - yOut;
			var isNorth = 	(dYOut == 1 && dXOut == 0);
			var isSouth = 	(dYOut == -1 && dXOut == 0);
			var isEast = 	(dXOut == -1 && dYOut == 0);
			var isWest = 	(dXOut == 1 && dYOut == 0);
			if (isEast) rotation._ = 0;
			else if (isWest) rotation._ = 180;
			else if (isNorth) rotation._ = 270;
			else if (isSouth) rotation._ = 90;
			
			texture = GV.tPack.getTexture("snake_tail");
		}
		
		// angle
		else {
			var dXIn = xTile - xIn;
			var dYIn = yTile - yIn;
			var dXOut = xTile - xOut;
			var dYOut = yTile - yOut;
			var isNorth = 	( (dYIn == -1 && dXIn == 0) || (dYOut == -1 && dXOut == 0));
			var isSouth = 	( (dYIn == 1 && dXIn == 0) 	|| (dYOut == 1 && dXOut == 0));
			var isEast = 	( (dXIn == 1 && dYIn == 0) 	|| (dXOut == 1 && dYOut == 0));
			var isWest = 	( (dXIn == -1 && dYIn == 0) || (dXOut == -1 && dYOut == 0));
			
			if (isSouth && isEast) 
				rotation._ = 90;
			else if (isSouth && isWest) 
				rotation._ = 180;
			else if (isNorth && isEast) 
				rotation._ = 0;
			else if (isNorth && isWest) 
				rotation._ = 270;
		}
	}
	
	override public function explode(g:Int) 
	{
		super.explode(g);
		
		var previous:Block = scene.getBlock(xPrev, yPrev);
		var next:Block = scene.getBlock(xNext, yNext);
		
		// transform previous in head
		if ( previous != null && previous.type == BlockType.snake_body) {
			var p = cast(previous, SnakeBody);
			p.transformToHead(p.xPrev, p.yPrev);
		}
		
		// transform next in tail
		if(next != null && next.type == BlockType.snake_body) {
			var n = cast (next, SnakeBody);
			n.setPreviousPosition(n.xNext, n.yNext, -1, -1);
		}
	}
	
	public function chainExplode(g:Int) 
	{
		if (g > 1024) g = 1024; 
		super.explode(g);

		var block = scene.getBlock(xPrev, yPrev);
		if (block == null) return;
		
		// SnakeBody 
		if(block.type == BlockType.snake_body) {
			var body:SnakeBody = cast(block, SnakeBody);
			body.chainExplode(g*2);
		}
	}
	
	function transformToHead(xPrev:Int, yPrev:Int) 
	{
		var h = new SnakeHead(scene, xTile, yTile);
		h.setPreviousPosition(xPrev, yPrev);
		scene.addBlock(xTile, yTile, h);
	}
	
}