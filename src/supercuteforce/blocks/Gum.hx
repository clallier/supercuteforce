package supercuteforce.blocks;

import flambe.display.ImageSprite;
import flambe.Entity;
import supercuteforce.GV;
import supercuteforce.scene.GameScene;
import supercuteforce.blocks.BlockFactory.BlockType;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Gum extends Block
{

	public function new(scene:GameScene, x:Int, y:Int) 
	{
		super(GV.tPack.getTexture("gum"), scene, x, y);
		this.type = BlockType.gum;
	}
	
	override public function copy(xTile:Int, yTile:Int) :Block {
		var b = new Gum(scene, xTile, yTile);
		return b;
	}

	
	override public function reemit()
	{
		//emit new bullets
		for (i in -1...2) {
			scene.addBullet(xTile, yTile, i * 90, 0, weight);
		}
		
		var b = scene.getBlock(xTile, yTile + 1);
		if (b != null && b.getEmittedType() == this.type)
			scene.addBullet(xTile, yTile, 180, type, weight);
			
		super.reemit();
	}

	
}