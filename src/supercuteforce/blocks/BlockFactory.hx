package supercuteforce.blocks;

import supercuteforce.blocks.Block;
import supercuteforce.blocks.BubbleBlue;
import supercuteforce.blocks.BubbleGreen;
import supercuteforce.blocks.BubblePink;
import supercuteforce.blocks.BubbleRed;
import supercuteforce.blocks.Gum;
import supercuteforce.blocks.Lollipop;
import supercuteforce.scene.GameScene;

class BlockType {
	public static inline var none:Int = 0;
	public static inline var bubble_blue:Int = 1;
	public static inline var bubble_green:Int = 2; 
	public static inline var bubble_pink:Int = 3;
	public static inline var bubble_red:Int = 4;
	public static inline var gum:Int = 5;
	public static inline var lollipop:Int = 6;
	public static inline var snake_head:Int = 7;
	public static inline var snake_body:Int = 8;
	public static inline var rock:Int = 9;
	
	public static inline var bubble_max_idx = bubble_red;
}

class LootType {
	public static inline var bomb:Int = 1;
	public static inline var ruppie:Int = 2;
}

/**
 * ...
 * @author Godjam - QilinEggs
 */
class BlockFactory
{
	public static function create(scene:GameScene, xTile:Int, yTile:Int, type:Int ) : Block 
	{
		if (type == BlockType.bubble_blue)
			return new BubbleBlue(scene, xTile, yTile);
		
		if (type == BlockType.bubble_green)
			return new BubbleGreen(scene, xTile, yTile);
			
		if (type == BlockType.bubble_pink)
			return new BubblePink(scene, xTile, yTile);
			
		if (type == BlockType.bubble_red)
			return new BubbleRed(scene, xTile, yTile);
		
		else if (type == BlockType.gum) 
			return new Gum(scene, xTile, yTile);
		
		else if (type == BlockType.lollipop)
			return new Lollipop(scene, xTile, yTile);
		
		else if (type == BlockType.snake_head)
			return new SnakeHead(scene, xTile, yTile);
		
		else if (type == BlockType.snake_body)
			return new SnakeBody(scene, xTile, yTile);
			
		else if (type == BlockType.rock)
			return new Rock(scene, xTile, yTile);
		
		else return null; // if type == 0 for instance
 	}
	
	static public function createRandomBlock(scene:GameScene, xTile:Int, yTile:Int, max:Int) :Block
	{
		return create(scene, xTile, yTile, Math.floor(Math.random() * max + 1));
	}
	
	public static function createLoot(scene:GameScene, xTile:Int, yTile:Int, type:Int ) : Loot
	{
		if (type == LootType.bomb)
			return new Bomb(scene, xTile, yTile);
		
		else /*if (type == LootType.ruppie)*/
			return new Ruppie(scene, xTile, yTile);
	}
}