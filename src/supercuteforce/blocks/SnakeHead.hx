package supercuteforce.blocks;

import flambe.display.Texture;
import flambe.math.Point;
import flambe.script.Action;
import flambe.script.Script;
import supercuteforce.scene.GameScene;
import flambe.script.Repeat;
import flambe.script.Sequence;
import flambe.script.Delay;
import flambe.script.CallFunction;
import flambe.Entity;
import supercuteforce.blocks.BlockFactory.BlockType;
import supercuteforce.actions.SnakeMover;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class SnakeHead extends Block
{
	public var xPrev(default, null) = -1;
	public var yPrev(default, null) = -1;
	
	public function new(scene:GameScene, x:Int, y:Int) 
	{
		super(GV.tPack.getTexture("snake_head"), scene, x, y);
		this.type = BlockType.snake_head;
	}
	
	override public function copy(xTile:Int, yTile:Int) : Block
	{
		var dX = xTile - this.xTile; 
		var dY = yTile - this.yTile; 
		var b:SnakeHead = new SnakeHead(scene, xTile, yTile);
		b.setPreviousPosition(xPrev+dX, yPrev+dY);
		return b;
	}
	
	
	override public function onAdded() {
		super.onAdded();
		script.run(new Repeat(new Sequence([
			new Delay(1),
			new CallFunction(execute)])));
	}
	
	public function setPreviousPosition(prevXTile:Int, prevYTile:Int) 
	{
		xPrev = prevXTile;
		yPrev = prevYTile;
		
		var dXTile = xTile - prevXTile;
		var dYTile = yTile - prevYTile;
			
		if (dYTile == 1)
			rotation._ = 180;
			
		if (dXTile == -1)
			rotation._ -= 90;
		
		if (dXTile == 1)
			rotation._ += 90;
	}
	
	function execute() {
		scene.addAction(new SnakeMover(scene, this));
	}
		
	override public function reemit()
	{
		// set the chain to explode
		var block = scene.getBlock(xPrev, yPrev);
		if (block == null || block.type != BlockType.snake_body) return;
		
		var body:SnakeBody = cast(block, SnakeBody);
		body.chainExplode(weight);
		
		super.reemit();
	}
	
}