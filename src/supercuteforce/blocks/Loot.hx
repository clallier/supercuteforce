package supercuteforce.blocks;

import flambe.display.ImageSprite;
import flambe.display.Texture;
import supercuteforce.scene.GameScene;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Loot extends ImageSprite
{
	var scene:GameScene;
	var xTile:Int = 0;
	var yTile:Int = 0;
	var ttl:Float = 1;
	
	public function new(scene:GameScene, xTile:Int, yTile:Int, texture:Texture) 
	{
		super(texture);
		setXY(xTile * GV.k, yTile * GV.k);
		this.scene = scene;
		this.xTile = xTile;
		this.yTile = yTile;
		y.animateTo(scene.mapHeightInPx()-GV.k*0.5, ttl);
	}
	
	override public function onUpdate(dt:Float) {
	
		super.onUpdate(dt);
		
		// get xTile & yTile
		xTile = Std.int(x._ / GV.k);
		yTile = Std.int(y._ / GV.k);
		
		if (ttl <= 0)
			delete();
			
		// if xTile & yTile are the same as player up or down => explode & kill player
		if (scene.getPlayer().collideWith(xTile, yTile)) {
			pickUp();
		}
			
		ttl -= dt;
	}
	
	function delete() 
	{
		scene.deleteMovingObject(this);
	}
	
	function pickUp():Void 
	{
		delete();
	}
}