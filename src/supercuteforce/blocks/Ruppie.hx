package supercuteforce.blocks;
import flambe.animation.Ease;
import flambe.display.Texture;
import supercuteforce.scene.GameScene;
import flambe.display.ImageSprite;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Ruppie extends Loot
{
	var score:Int;
	
	public function new(scene:GameScene, xTile:Int, yTile:Int) 
	{
		score = 50;
		
		ttl = 2;
		super(scene, xTile, yTile, GV.tPack.getTexture("ruppie_red"));
		y.animateTo(scene.mapHeightInPx(), ttl, Ease.bounceOut);
	}
	
	override function pickUp() 
	{
		GV.bPack.getSound("ruppie").play();
		scene.addScoreText(xTile, yTile, score);
		scene.incrementScore(score);
		super.pickUp();
	}
}