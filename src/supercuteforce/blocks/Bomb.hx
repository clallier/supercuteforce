package supercuteforce.blocks;
import flambe.display.Sprite;
import flambe.display.ImageSprite;
import supercuteforce.scene.GameScene;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.script.MoveTo;
import flambe.script.CallFunction;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Bomb extends Loot
{
	public function new(scene:GameScene, xTile:Int, yTile:Int) 
	{
		super(scene, xTile, yTile, GV.tPack.getTexture("bomb"));
	}
	
	override function delete() 
	{
		GV.bPack.getSound("bomb_explode").play();
		scene.addEmitter(x._, y._);
		super.delete();
	}
	
	override function pickUp() {
		GV.bPack.getSound("bomb_explode").play();
		scene.killPlayer();
		super.pickUp();
	}
}