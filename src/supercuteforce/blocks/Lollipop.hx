package supercuteforce.blocks;

import flambe.display.Texture;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Repeat;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.scene.GameScene;
import supercuteforce.blocks.BlockFactory.BlockType;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Lollipop extends Block
{
	var texture_red:Texture;
	var texture_green:Texture;
	var texture_pink:Texture;
	var texture_blue:Texture;
	var emittedType = 0;
	
	public function new(scene:GameScene, x:Int, y:Int) 
	{
		texture_red = GV.tPack.getTexture("lollipop_red");
		texture_green = GV.tPack.getTexture("lollipop_green");
		texture_pink = GV.tPack.getTexture("lollipop_pink");
		texture_blue = GV.tPack.getTexture("lollipop_blue");
		
		super(texture_pink, scene, x, y);
		this.type = BlockType.lollipop;
		emittedType = BlockType.bubble_pink;
		execute();
	}
	
	override public function copy(xTile:Int, yTile:Int) :Block {
		var b = new Lollipop(scene, xTile, yTile);
		return b;
	}
	
	override public function onAdded() {
		super.onAdded();
		script.run(new Repeat(new Sequence([new Delay(4), new CallFunction(execute)])));
	}
	
	function execute() {
		var array:Array<Block> = new Array<Block>();
		
		var t = scene.getBlock(xTile, yTile-1);
		if (t != null && t.type <= BlockType.bubble_max_idx) array.push(t);
		
		var t = scene.getBlock(xTile, yTile+1);
		if (t != null && t.type <= BlockType.bubble_max_idx) array.push(t);
		
		var t = scene.getBlock(xTile-1, yTile);
		if (t != null && t.type <= BlockType.bubble_max_idx) array.push(t);
		
		var t = scene.getBlock(xTile + 1, yTile);
		if (t != null && t.type <= BlockType.bubble_max_idx) array.push(t);
		
		if (array.length == 0) return;
			
		var b:Block = array[Std.int(Math.random() * array.length)];
		
		emittedType = b.type;
		if (emittedType  == BlockType.bubble_blue)
			texture = texture_blue;
		else if (emittedType  == BlockType.bubble_green)
			texture = texture_green;
		else if (emittedType  == BlockType.bubble_pink)
			texture = texture_pink;
		else if (emittedType  == BlockType.bubble_red)
			texture = texture_red;
				
		centerAnchor();
		setScale(GV.scaleTo(GV.k, getNaturalWidth()));
	}
	
	override public function reemit()
	{
		//emit new bullets
		for (i in -2...3) {
			scene.addBullet(xTile, yTile, i * 45, emittedType, weight);
		}
		
		var b = scene.getBlock(xTile, yTile + 1);
		if (b != null && b.getEmittedType() == this.type)
			scene.addBullet(xTile, yTile, 180, type, weight);
		
		super.reemit();
	}
	
	override public function getEmittedType()
	{
		return emittedType;
	}
}