package supercuteforce.blocks;
import flambe.display.Texture;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Repeat;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.blocks.BlockFactory.BlockType;
import supercuteforce.blocks.BlockFactory.LootType;
import supercuteforce.GV;
import supercuteforce.scene.GameScene;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class BubbleRed extends Block
{
	var textures:Array<Texture>;
	var t:Int = 0;
		
	public function new(scene:GameScene, x:Int, y:Int) 
	{
		textures = new Array<Texture>();
		textures.push(GV.tPack.getTexture("bubble_red_1"));
		textures.push(GV.tPack.getTexture("bubble_red_2"));
		super(textures[t], scene, x, y);
		this.type = BlockType.bubble_red;
	}
	
	override public function copy(xTile:Int, yTile:Int) :Block {
		var b = new BubbleRed(scene, xTile, yTile);
		return b;
	}
	
	override public function reemit()
	{
		//emit new bullets
		for (i in -1...2) {
			scene.addBullet(xTile, yTile, i * 90, type, weight);
		}
				
		var b = scene.getBlock(xTile, yTile + 1);
		if (b != null && b.getEmittedType() == this.type)
			scene.addBullet(xTile, yTile, 180, type, weight);
			
		super.reemit();
	}
	
	override public function onAdded() {
		super.onAdded();
		script.run(new Repeat(new Sequence([new Delay(1), new CallFunction(execute)])));
	}
	
	function execute() {
		++t;
		if (t >= textures.length) t = 0;
		texture = textures[t];
		centerAnchor();
		setScale(GV.scaleTo(GV.k, getNaturalWidth()));
	}
	
	override public function dropLoot() {
		if (Math.random() < 0.25) // 1/4
			scene.addLoot(LootType.bomb, xTile, yTile);
	}
}