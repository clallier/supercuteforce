package supercuteforce.blocks;

import flambe.Component;
import flambe.display.ImageSprite;
import flambe.display.TextSprite;
import flambe.display.Texture;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.script.Shake;
import supercuteforce.actions.Gravity;
import supercuteforce.scene.GameScene;
import supercuteforce.blocks.BlockFactory.BlockType;
import flambe.animation.Ease;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Block extends ImageSprite
{
	var scene :GameScene;

	public var f:Float = 0;
	public var h:Float = 0;
	public var g:Float = 0;
	public var parent:Block = null;
	public var type(default, null):Int;
	public var xTile(default, null):Int; // Tile
	public var yTile(default, null):Int; // Tile
	var script:Script;
	var weight:Int= 0;
	
	public function new(texture:Texture, scene:GameScene, x:Int, y:Int) 
	{
		this.visible = false;
		this.xTile = x;
		this.yTile = y;
		this.scene = scene;
		super(texture);
		centerAnchor();
		setXY(xTile * GV.k + GV.k*.5, yTile * GV.k + GV.k*.5);
		setScale(1.0 / getNaturalWidth() * GV.k);
		script = new Script();
	}
	
	public function copy(xTile:Int, yTile:Int) :Block {
		var b = new Block(texture, scene, xTile, yTile);
		return b;
	}
	
	override public function onAdded()
	{
		owner.add(script);
	}

	public function explode(g:Int) 
	{
		scene.addSmallEmitter(x._, y._);
		scene.deleteBlock(this);
		playSound(g);
		scene.slomoEffect();
		this.weight = g;
		var actions = new Sequence([new Delay(0.060), new CallFunction(reemit), new CallFunction(dropLoot), new CallFunction(disposeAsMovingObject)]);
		scene.addMovingObject(this, actions);
				
		// score text 
		scene.addScoreText(xTile, yTile, g); 
		
		// shake !
		scene.shakeScreen(g);
	}
	
	public function disposeAsBlock() {
		scene.deleteBlock(this);
	}
	
	public function disposeAsMovingObject() {
		scene.deleteMovingObject(this);
	}
	
	public function getEmittedType() 
	{
		return type;
	}
	
	public function reemit() { }
	public function dropLoot() { }
	
	public function moveTo(xTile:Int, yTile:Int, xTile1:Int, yTile1:Int, time:Float) 
	{
		x.animate(xTile * GV.k+GV.k*.5, xTile1 * GV.k+GV.k*.5, time, Ease.quadOut);
		y.animate(yTile * GV.k+GV.k*.5, yTile1 * GV.k+GV.k*.5, time, Ease.quadOut);
	}
	
	function playSound(g:Int):Void 
	{
		g = Math.round(Math.sqrt(g));
		if (g > 14) g = 12;
		var sndName:String = "pling" + g;
		GV.bPack.getSound(sndName).play();
	}
}