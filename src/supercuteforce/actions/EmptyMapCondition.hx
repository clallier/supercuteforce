package supercuteforce.actions;
import flambe.script.Action;
import flambe.Entity;
import supercuteforce.scene.GameScene;
import flambe.System;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class EmptyMapCondition implements Action
{
	var scene:GameScene;
	
	public function new(scene:GameScene) 
	{
		this.scene = scene;
	}
	
	/* INTERFACE flambe.script.Action */
	
	public function update(dt:Float, actor:Entity):Float 
	{
		if (scene.isActive() && scene.hasBlock() == false) {
			scene.clearActionsQueue();
			scene.runEndWithVictory();
		}
		return dt;
	}
}