package supercuteforce.actions;
import flambe.script.Action;
import supercuteforce.scene.GameScene;
import supercuteforce.blocks.Block;
import flambe.System;
import flambe.Entity;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Gravity implements Action
{
	var scene:GameScene;
	var xTile:Int;
	var yTile:Int;
	var openList:Array<Block>;
	var closedList:Array<Block>;
	var modified = 0;
	
	public function new(scene:GameScene) {
		this.scene = scene;
	}
	
	public function update(dt:Float, actor:Entity):Float 
	{
		for (y in 0...GV.mapHeight ) {
			for (x in 0...GV.mapWidth ) {
				if ( /*x == 0 &&*/ y == 0) continue;
				var b:Block = scene.getBlock(x, y);
				
				if (b != null) {
					if(search(b).length == 0) {//not connected => detach
						propagateDetach(b, 1);
						++modified;
					}
					cleanupVisited();
				}
			}
		}
		return dt;
	}
	
	function search(b:Block) : Array<Block>
	{
		openList = new Array<Block>();
		closedList = new Array<Block>();
		openList.push(b);
		
		while (openList.length > 0) {
			
			// Grab the lowest f(x) to process next
			var lowInd = 0;
			for( i in 0...openList.length) {
				if(openList[i].f < openList[lowInd].f) { lowInd = i; }
			}
			var currentNode = openList[lowInd];
			
			// End case -- result has been found, return the traced path
			if(currentNode.yTile == 0) {
				var curr = currentNode;
				var ret:Array<Block> = new Array<Block>();
				ret.push(curr);
				while(curr.parent != null) {
					ret.push(curr);
					curr = curr.parent;
				}
				ret.reverse();
				return ret;
			}
			
			// Normal case -- move currentNode from open to closed, process each of its neighbors
			openList.remove(currentNode);
			closedList.push(currentNode);
			var neighbors:Array<Block> = getNeighbors(currentNode);
 
			for(i in 0 ... neighbors.length) {
				var neighbor = neighbors[i];
				// not a valid node to process, skip to next neighbor
				if (isInClosedList(neighbor)) continue;
				
				// g score is the shortest distance from start to current node, we need to check if
				//	 the path we have arrived at this neighbor is the shortest one we have seen yet
				var gScore = currentNode.g + 1; // 1 is the distance from a node to it's neighbor
				var gScoreIsBest = false;
				
				if(!isInOpenList(neighbor)) {
					// This the the first time we have arrived at this node, it must be the best
					// Also, we need to take the h (heuristic) score since we haven't done so yet
 
					gScoreIsBest = true;
					neighbor.h = heuristic(neighbor.xTile, neighbor.yTile, neighbor.xTile, 0);
					openList.push(neighbor);
				}
				
				else if(gScore < neighbor.g) {
					// We have already seen the node, but last time it had a worse g (distance from start)
					gScoreIsBest = true;
				}
				
				if(gScoreIsBest) {
					// Found an optimal (so far) path to this node.	 Store info on how we got here and
					//	just how good it really is...
					neighbor.parent = currentNode;
					neighbor.g = gScore;
					neighbor.f = neighbor.g + neighbor.h;
				}
			}
		}
		
		return new Array<Block>();
	}
	
	function getNeighbors(b:Block) : Array<Block>
	{
		var neighbors = new Array<Block>();
		// T&D neigbors
		var n = scene.getBlock(b.xTile, b.yTile-1);
		if(n != null) neighbors.push(n);
		var n = scene.getBlock(b.xTile, b.yTile+1);
		if(n != null) neighbors.push(n);

		
		// L&R neigbors
		var n = scene.getBlock(b.xTile-1, b.yTile);
		if(n != null) neighbors.push(n);
		var n = scene.getBlock(b.xTile+1, b.yTile);
		if (n != null) neighbors.push(n);
		
		return neighbors;
	}
	
	function isInClosedList(b:Block) : Bool
	{
		for (i in 0 ... closedList.length)
			if (closedList[i] == b)
				return true;
		
		return false;
	}
	
	
	function isInOpenList(b:Block) : Bool
	{
		for (i in 0 ... openList.length)
			if (openList[i] == b)
				return true;
		
		return false;
	}
	
	function heuristic(x1:Int,y1:Int, x2:Int, y2:Int) : Float
	{
		var d1 = Math.abs (x2 - x1);
		var d2 = Math.abs (y2 - y1);
		return d1 + d2; //manhattan dist
	}
	
	function cleanupVisited()
	{
		for (i in 0 ...GV.mapWidth)
			for (j in 0 ... GV.mapHeight) {
				var b:Block = scene.getBlock(i, j);
				if (b != null) {
					b.f = 0;
					b.g = 0;
					b.h = 0;
					b.parent = null;
				}
			}
	}
	
	public function propagateDetach(b:Block, g:Int) 
	{
		if(g < 1024)
			g = g*2;
		scene.detachBlock(b, g);
		var xTile = b.xTile;
		var yTile = b.yTile;
		
		var neigbors = new Array<Block>();
		//T&D neigbors
		neigbors.push(scene.removeBlock(xTile, yTile+1));
		neigbors.push(scene.removeBlock(xTile, yTile-1));
		// L&R neigbors
		neigbors.push(scene.removeBlock(xTile-1, yTile));
		neigbors.push(scene.removeBlock(xTile+1, yTile));	
			
		// propagate deconnection
		for(i in 0 ...neigbors.length) {
			b = neigbors[i];
			if (b != null) propagateDetach(b, g);
		}
	}
	
	public function hasModifiedMap():Int 
	{
		return modified;
	}
}