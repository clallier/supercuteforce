package supercuteforce.actions;

import flambe.script.Action;
import flambe.Entity;
import supercuteforce.blocks.SnakeBody;
import supercuteforce.blocks.SnakeHead;
import supercuteforce.scene.GameScene;
import supercuteforce.blocks.Block;
import supercuteforce.blocks.BlockFactory.BlockType;
import flambe.System;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class SnakeMover implements Action
{
	var scene:GameScene;
	var head:SnakeHead;
	
	public function new(scene:GameScene, head:SnakeHead) {
		this.scene = scene;
		this.head = head;
	}
	
	public function update(dt:Float, actor:Entity):Float 
	{
		if (Math.random() < 0.1) return 0; // 10% of chance of doing nothing
		
		var array = new Array<Block>();
		var usableBlocks = new Array<Block>();
		var xTile = head.xTile;
		var yTile = head.yTile;
		var xPrev = head.xPrev;
		var yPrev = head.yPrev;
		
		if (xTile > 0)
			array.push(scene.getBlock(xTile-1, yTile));
		if (xTile < GV.mapWidth)
			array.push(scene.getBlock(xTile+1, yTile));
		if (yTile > 0)
			array.push(scene.getBlock(xTile, yTile-1));
		if (yTile < GV.mapHeight)
			array.push(scene.getBlock(xTile, yTile+1));

		for (i in 0...array.length) {
			if (array[i] == null) continue;
			if (array[i].type != BlockType.snake_head 
			&& array[i].type != BlockType.snake_body
			&& array[i].type != BlockType.rock)
				usableBlocks.push(array[i]);
		}
		
		if(usableBlocks.length >= 1) {
			var i = Std.int(Math.random() * usableBlocks.length);
			var eatedBlock = usableBlocks[i];
			var xEated = eatedBlock.xTile;
			var yEated = eatedBlock.yTile;
			
			// place new body block
			scene.createAndAddBlock(xTile, yTile, BlockType.snake_body, xPrev, yPrev);
			var b:SnakeBody = cast(scene.getBlock(xTile, yTile), SnakeBody);
			if(b != null)
				b.setPreviousPosition(xEated, yEated, xPrev, yPrev);
				
			// place head
			scene.createAndAddBlock(xEated, yEated, BlockType.snake_head, xPrev, yPrev);
			var h:SnakeHead = cast(scene.getBlock(xEated, yEated), SnakeHead);
			if(h != null)
				h.setPreviousPosition(xTile, yTile);
		}
		
		return dt;
	}
}