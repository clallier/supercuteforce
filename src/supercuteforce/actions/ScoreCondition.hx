package supercuteforce.actions;

import flambe.script.Action;
import flambe.Entity;
import supercuteforce.scene.GameScene;
import flambe.System;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class ScoreCondition implements Action
{
	var scene:GameScene;
	var score:Int;
	
	public function new(scene:GameScene, score:Int) 
	{
		this.scene = scene;
		this.score = score;
	}
	
	/* INTERFACE flambe.script.Action */
	public function update(dt:Float, actor:Entity):Float 
	{
		if (scene.isActive() && scene.getHud().getScore() >= score) {
			scene.clearActionsQueue();
			scene.runEndWithVictory();
		}
		
		//empty map
		if (scene.isActive() && scene.hasBlock() == false) {
			scene.initNewLines(8, GV.max_types);
			GV.bPack.getSound("weee").play();
			scene.getHud().incrementScore(1000);
		}
		return dt;
	}
	
}