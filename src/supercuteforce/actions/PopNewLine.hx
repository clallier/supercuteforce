package supercuteforce.actions;

import flambe.Entity;
import flambe.script.Action;
import supercuteforce.scene.GameScene;
import supercuteforce.GV;
import flambe.System;
import supercuteforce.blocks.BlockFactory.BlockType;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class PopNewLine implements Action
{
	var scene:GameScene;
	
	public function new(scene:GameScene) 
	{
		this.scene = scene;
	}
	
	/* INTERFACE flambe.script.Action */
	
	public function update(dt:Float, actor:Entity):Float 
	{
		var hiPt = -1;
		var map = scene.getMap();
		
		// move down 
		//trace("move down");
		for (y in 1 ...GV.mapHeight) {
			var ny = GV.mapHeight - y;
			
			for (x in 0...GV.mapWidth) {
				if (map[ny - 1][x] != null) {
					var type = map[ny - 1][x].type;
					
					if (type == 0) continue;	
					//trace("moving", x, ny, type);
					scene.cutPasteBlock(x, ny - 1, x, ny);
					
					if (ny > hiPt) hiPt = ny;
				}
			}
		}	
		
		// adding new line
		//trace("generate");
		var existingTypes:Array<Int> = new Array<Int>();
		for (y in 0...GV.mapHeight) {
			for (x in 0...GV.mapWidth) {
				var block = scene.getBlock(x, y);
				if (block == null) continue;
				
				var type = block.type;
				if (type == BlockType.none || 
					type == BlockType.snake_body || 
					type == BlockType.snake_head) continue;
				
				existingTypes.remove(type);
				existingTypes.push(type);
			}
		}
		
		for (x in 0...GV.mapWidth)
			scene.createAndAddExistingBlock(x, 0, existingTypes);
		
		if (hiPt == GV.mapHeight-2)
			scene.runEndWithLose();
	return dt;
	}
	
}