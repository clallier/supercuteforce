package supercuteforce.actions;
import supercuteforce.scene.GameScene;
import flambe.script.Action;
import flambe.Entity;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class SwapBuffer implements Action
{
	var scene:GameScene;
	
	public function new(scene:GameScene) 
	{
		this.scene = scene;
	}
	
	/* INTERFACE flambe.script.Action */
	public function update(dt:Float, actor:Entity):Float 
	{
		scene.swapMap();
		return dt;
	}
}