package supercuteforce.backgrounds;
import flambe.display.PatternSprite;
import flambe.Entity;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class TokyoCityBackground extends Background
{
	var bg:PatternSprite;
	var city:PatternSprite;
	var fg1:PatternSprite;
	
	public function new() 
	{
		createTexture();
		super();
	}
	
	override function createTexture(withResize:Bool = false):Void 
	{
		bg = new PatternSprite(GV.tPack.getTexture("colorcity_bg"), GV.width, GV.height);
		city = new PatternSprite(GV.tPack.getTexture("tokyocity"), GV.width);
		fg1 = new PatternSprite(GV.tPack.getTexture("tokyocity_fg1"), GV.width);
			
		city.y._ = GV.height -  city.getNaturalHeight();
		
		super.createTexture(withResize);
	}
	override public function onAdded() {
		super.onAdded();
		if(owner != null) {
			owner.addChild(new Entity().add(bg));
			owner.addChild(new Entity().add(city));
			owner.addChild(new Entity().add(fg1));	
		}
	}
	
	
	override public function resize(width:Float, height:Float) 
	{ 
		reSize.x = width;
		reSize.y = height;
		bg.setSize(width, height);
		trace("resized tokyoCity to "+ width +", "+ height);
	}
}