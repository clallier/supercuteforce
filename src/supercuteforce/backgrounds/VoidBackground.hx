package supercuteforce.backgrounds;
import flambe.debug.FpsDisplay;
import flambe.display.FillSprite;
import flambe.display.ImageSprite;
import flambe.display.PatternSprite;
import flambe.display.Sprite;
import flambe.Entity;
import flambe.math.Point;
import flambe.System;
import supercuteforce.GV;
import flambe.display.BlendMode;
import supercuteforce.TrailSprite;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class VoidBackground extends Background
{
	var time:Float = 0;
	var size:Point;
	//var buff:FillSprite;
	//var buff:ImageSprite;
	var buff:PatternSprite;
	var a:Float;
	var b:Float;
	var r:Float;
	var fade:Float;
	var radius:Float;
	var e:Float;
	var p_x:Float;
	var p_y:Float;
	var x:Float;
	var y:Float;
	var c:Array<Float>;
	var d:Array<Float>;
	var angle:Float;
	var color:Int;
	var limit1:Float;
	var limit2:Float;
	var prv_x:Float;
	var prv_y:Float; 
	var prv_x2:Float; 
	var prv_y2:Float;
	var trails:Array<TrailSprite>;
	
	var debug:Float = 0;
	var timer:Float = 1;
	
	public function new() 
	{
		size = new Point(32, 32);
		createTexture();
		trails = new Array<TrailSprite>();
		for (i in 0...8)
			trails.push(new TrailSprite("additive", BlendMode.Add, 5, 4, 0.1));
		super();
	}
	
	override public function onAdded() {
		super.onAdded();
		if (owner != null) {
			owner.addChild(new Entity().add(buff));
			//owner.addChild(new Entity().add(new FpsDisplay()));
		
			for(i in 0...trails.length)
				owner.addChild(new Entity().add(trails[i]));
		}
		
		angle = Math.PI / 180 * 8;
		color = 0;
		limit1 = Math.PI * 1.5;
		limit2 = Math.PI * 1.79;
		c = new Array<Float>();
		d = new Array<Float>();
		a = Math.random()*angle;
		b = Math.random()*angle;
		r = 0;
		fade = 32;
		for(i in 0...6) {
			c[i]=Math.random()/2;
			d[i]=Math.random()/2;
		}
		
		radius=Math.round((reSize.x+reSize.y)/8);
		e = radius*0.2; /* 0.15 */
		p_x = Math.round(reSize.x/2);
		p_y = Math.round(reSize.y/2);
		x = (radius*c[0])*Math.cos(a*d[1])+(radius*c[2])*Math.sin(a*d[3])+(radius*c[4])*Math.sin(a*d[5]);
		y = (radius*c[5])*Math.sin(a*d[4])+(radius*c[3])*Math.cos(a*d[2])+(radius*c[1])*Math.cos(a*d[0]);

	}
	
	override public function onUpdate(dt:Float)
	{
		super.onUpdate(dt);
		
		time += dt;
		var k = .1;
		
		if (buff.texture == null) {
			createTexture(true);
			return;
		}
		
		var a1=Math.cos(a*2);
		var a2=Math.cos(a*4);
		var a3=Math.cos(a);
		var a4=Math.sin(a);
		if(b>limit1&&b<limit2)
		{
			r+=radius*0.02*a1;
			prv_x=x;
			prv_y=y;
			x=prv_x2+r*a3;
			y=prv_y2+r*a4;
		} else {
			prv_x=x;
			prv_y=y;
			prv_x2=x;
			prv_y2=y;
			x=(radius*c[0])*Math.cos(a*d[1])+(radius*c[2])*Math.sin(a*d[3])+(radius*c[4])*Math.sin(a*d[5]);
			y=(radius*c[5])*Math.sin(a*d[4])+(radius*c[3])*Math.cos(a*d[2])+(radius*c[1])*Math.cos(a*d[0]);
		}
		var c3 = 16*Math.cos(a*10);
		var c1 = Math.floor(56*Math.cos(a*angle*4)+c3);
		var c2 = Math.floor(56 * Math.sin(a * angle * 4) - c3);
		var dX = Math.abs(prv_x - x);
		var dY = Math.abs(prv_y - y);
		var sp = 1 +((dX + dY) / 8);
		//trace(sp);
		/*
		debug++;
		debug %= GV.width;
		trails[0].addElement(debug, GV.height/2, 1);
		/*/
		trails[0].addElement(p_x + prv_x, p_y + prv_y, sp);
		trails[1].addElement(p_x - prv_x, p_y + prv_y, sp);
		trails[2].addElement(p_x - prv_x, p_y - prv_y, sp);
		trails[3].addElement(p_x + prv_x, p_y - prv_y, sp);
		trails[4].addElement(p_x + prv_y, p_y + prv_x, sp);
		trails[5].addElement(p_x - prv_y, p_y + prv_x, sp);
		trails[6].addElement(p_x - prv_y, p_y - prv_x, sp);
		trails[7].addElement(p_x + prv_y, p_y - prv_x, sp);
		//*/

		
		a+=angle*Math.cos(b);
		b+=angle*0.1;
		if(b>limit1)
		{
			//context.fillStyle='rgba(0,0,0,0.08)';
			//context.fillRect(0,0,reSize.x,reSize.y);
		}
		//if (b < limit2) timeout = setTimeout('anim()', fps); else reset();
		
		//from : http://www.bidouille.org/prog/plasma#2
		//for(y in 0...buff.texture.height)
			//for (x in 0...buff.texture.width) {
				/*var v:Float = 0.;
				var c:Point = new Point(x * k - k / 2, y * k - k / 2); 
				v += Math.sin(c.x + time);
				v += Math.sin((c.y + time) / 2);
				v += Math.sin((c.x + c.y + time) / 2);
				var d:Point = new Point (k / 2 * Math.sin(time / 3) + c.x, k / 2 * Math.cos(time / 2)+c.y);
				v += Math.sin(Math.sqrt(d.x * d.x + d.y * d.y + 1) + time);
				v = v / 2;
			
				var b = Math.floor(Math.cos(Math.PI * v) * 35)+220;
				var g = Math.floor(Math.sin(Math.PI * v) * 35)+220;
				var r = 255;*/
				//var i = x / buff.texture.width;
				//var j = y / buff.texture.height;
				/*var color = 0.0;
				color += Math.sin( i * Math.cos( time / 15.0 ) * 80.0 ) + Math.cos( j * Math.cos( time / 15.0 ) * 10.0 );
				color += Math.sin( j * Math.sin( time / 10.0 ) * 40.0 ) + Math.cos( i * Math.sin( time / 25.0 ) * 40.0 );
				color += Math.sin( i * Math.sin( time / 5.0 ) * 10.0 )  + Math.sin( j * Math.sin( time / 35.0 ) * 80.0 );
				color *= Math.sin( time / 10.0 ) * 0.5;
				
				var r = Math.floor(color * 255);
				var g = Math.floor(color * 0.5 * 255);
				var b = Math.floor(Math.sin( color + time / 3.0 ) * 0.75 * 255);*/
				
				//var color = (r << 16) | (g << 8 ) | b;
				//buff.texture.graphics.fillRect(color , x, y, 1, 1);
			//}
			
			//if (time > 12 * Math.PI)
			//	time = 0;
				
				/*
			
		*/
	}
	
	override function createTexture(withResize:Bool = false):Void 
	{
		//buff = new FillSprite(0x333333, size.x, size.y);
		buff = new PatternSprite(GV.tPack.getTexture("colorcity_bg"));
		//buff.setSize(size.x, size.y);
		//buff = new ImageSprite(System.createTexture(Std.int(size.x), Std.int(size.y)));
		super.createTexture(withResize);
	}
	
	override public function resize(width:Float, height:Float) 
	{ 
		//if (width > 720) width = 720;
		//if (height > 1024) height = 1024;
		
		reSize.x = width;
		reSize.y = height;
		
		var w = GV.scaleTo(width, buff.getNaturalWidth());
		var h = GV.scaleTo(height, buff.getNaturalHeight());
		//buff.scaleX._ = w;
		//buff.scaleY._ = h; 
		buff.setSize(width, height);
		//trace("resized to ", width, height, sprite.getNaturalWidth(), w, h);
	}
}