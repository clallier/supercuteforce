package supercuteforce.backgrounds;
import flambe.display.ImageSprite;
import flambe.display.PatternSprite;
import flambe.Entity;
import flambe.display.BlendMode;
import flambe.script.AnimateBy;
import flambe.script.MoveBy;
import flambe.script.Repeat;
import flambe.script.Script;
import flambe.script.Sequence;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class ColorCityBackground extends Background
{
	var bg:PatternSprite;
	var city:ImageSprite;
	var fg1:ImageSprite;
	var fg2:ImageSprite;
	
	public function new() 
	{
		createTexture();
		super();
	}
	
	override function createTexture(withResize:Bool = false):Void 
	{
		bg = new PatternSprite(GV.tPack.getTexture("colorcity_bg"), GV.width, GV.height);
		city = new ImageSprite(GV.tPack.getTexture("colorcity"));
		fg1 = new ImageSprite(GV.tPack.getTexture("colorcity_fg1"));
		fg2 = new ImageSprite(GV.tPack.getTexture("colorcity_fg2"));
		super.createTexture(withResize);
	}
	
	override public function onAdded() {
		super.onAdded();
		if (owner != null) {
			owner.addChild(new Entity().add(bg));
			owner.addChild(new Entity().add(city));
			owner.addChild(new Entity().add(fg1));
			owner.addChild(new Entity().add(fg2));
			
			
			city.centerAnchor();
			city.setXY(GV.width * 0.5, GV.height * 0.5);
			
			fg1.centerAnchor();
			fg1.setXY(GV.width * 0.5, GV.height * 0.5-GV.k *4);
			
			fg2.centerAnchor();
			fg2.setXY(GV.width * 0.5, GV.height * 0.5+GV.k *4);
			fg2.blendMode = Add;
			
			// scripts
			var s1:Script = new Script();
			var s2:Script = new Script();
			var a = GV.k / 8;
			var b = GV.k / 4;
			city.owner.add(s1);
			s1.run(new Repeat(new Sequence([new AnimateBy(city.y, a, 4), 
											new AnimateBy(city.y, -a, 4) ])));
			fg1.owner.add(s2);
			s2.run(new Repeat(new Sequence([new AnimateBy(fg1.y, b, 2), 
											new AnimateBy(fg1.y, -b, 2) ])));
		}
	}
	
	override public function resize(width:Float, height:Float) 
	{ 
		reSize.x = width;
		reSize.y = height;
		bg.setSize(width, height);
		trace("resized colorCity to "+ width +", "+ height);
	}
}