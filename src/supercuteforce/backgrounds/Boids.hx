package supercuteforce.backgrounds;
import flambe.display.ImageSprite;
import flambe.display.BlendMode;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Boids extends ImageSprite
{
	public var vx:Float;
	public var vy:Float;
	
	public function new() 
	{
		super(GV.tPack.getTexture("additive"));
		centerAnchor();
		alpha._ = 0.3;
		this.x._=Math.random()*GV.width;
		this.y._=Math.random()*GV.height;
		this.vx=0;
		this.vy = 0;
		blendMode = BlendMode.Add;
	}
	
	public function move(){
		//the speed limit
		//if(this.vx>3)this.vx=3;
		//if(this.vx<-3)this.vx=-3;
		//if(this.vy>3)this.vy=3;
		//if(this.vy<-3)this.vy=-3;


		this.x._+=this.vx;
		this.y._+=this.vy;
		this.vx*=0.9;
		this.vy*=0.9;
		this.vx+=(Math.random()-0.5)*0.4;
		this.vy+=(Math.random()-0.5)*0.4;
		
		
		
		
		//tendenci to go towards center;)
		this.x._ =(this.x._*500+GV.width/2)/501;
		this.y._ = (this.y._* 500 + GV.height / 2) / 501;
		rotation.animateTo((angle(this.vx,this.vy)), 0.2);
	}

	function angle(x:Float,y:Float):Float{//the strangest of functions
		var angle:Float=0;
		if(x<0){
			angle+=Math.PI;
		}
		if(y<0){
			y=-y;
			x=-x;
		}
		angle+=Math.atan(y/x);
		return angle * 180 / Math.PI;
	}
	
}