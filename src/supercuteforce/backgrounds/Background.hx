package supercuteforce.backgrounds;

import flambe.Component;
import flambe.display.FillSprite;
import flambe.display.PatternSprite;
import flambe.display.Sprite;
import flambe.display.Texture;
import flambe.math.Point;
import flambe.Entity;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Background extends Component
{
	var reSize:Point;
	var fillSprite:FillSprite;
	
	public function new() 
	{	
		reSize = new Point(GV.width, GV.height);
    	resize(GV.width, GV.height);
		fillSprite = new FillSprite(0xffffff, GV.width, GV.height);
	}
	
	public function resize(width:Float, height:Float) 
	{ 
		//if (width > 720) width = 720;
		//if (height > 1024) height = 1024;
		
		reSize.x = width;
		reSize.y = height;
		
		//var w = GV.scaleTo(width, getNaturalWidth());
		//var h = GV.scaleTo(height, getNaturalHeight());
		//scaleX._ = w;
		//scaleY._ = h; 
		//trace("resized to ", width, height, sprite.getNaturalWidth(), w, h);
	}
	
	function createTexture(withResize:Bool = false):Void 
	{
		if (withResize)
			resize(reSize.x, reSize.y);
	}
	
	public function glitch() {
		if(owner != null) {
			if (fillSprite.owner != null) { fillSprite.owner.dispose(); } 
			fillSprite.color = ColorHelper.getRandomColor();
			owner.addChild(new Entity().add(fillSprite));
			fillSprite.alpha.animate(1, 0, 0.1);
		}
	}
}