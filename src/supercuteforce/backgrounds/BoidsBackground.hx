package supercuteforce.backgrounds;

import flambe.animation.AnimatedFloat;
import flambe.display.PatternSprite;
import flambe.Entity;
import flambe.math.Point;
import supercuteforce.backgrounds.Background;
import supercuteforce.backgrounds.Boids;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class BoidsBackground extends Background
{
	//the bank o fish
	var pesti:Array<Boids>;
	var mouse:Point;
	var timeout:AnimatedFloat;
	var buff:PatternSprite;
	
	public function new() 
	{
		super();
		buff = new PatternSprite(GV.tPack.getTexture("colorcity_bg"));
		buff.setSize(GV.width, GV.height);
		pesti = new Array<Boids>();
		for (i in 0...50)
			pesti.push(new Boids());
		mouse = new Point(Math.random() * GV.width, Math.random() * GV.height);
		timeout = new AnimatedFloat(0, mouseChanger);
		timeout.animate(0, 1, 2);
	}
	
	function mouseChanger(v1:Float, v2:Float) {
		//trace(v1, v2);
		if (v1 >= 0.99) {
			//trace(mouse);
			mouse = new Point(Math.random() * GV.width, Math.random() * GV.height);
			timeout.animate(0, 1, 2);
			//trace(mouse);
		}
	}
	
	override public function onAdded() 
	{
		super.onAdded();
		
		if (owner != null) {
			owner.addChild(new Entity().add(buff));
			for (i in 0...pesti.length)
				owner.addChild(new Entity().add(pesti[i]));
		}
	}
	
	override public function onUpdate(dt:Float)
	{
		super.onUpdate(dt);
		timeout.update(dt);
		for(x in 0...pesti.length){
			for(y in 0...pesti.length){
				if(y!=x){
					var dx=pesti[y].x._-pesti[x].x._;
					var dy=pesti[y].y._-pesti[x].y._;
					var d=Math.sqrt(dx*dx+dy*dy);
					if(d<40){
						pesti[x].vx+=20*(-dx/(d*d));
						pesti[x].vy+=20*(-dy/(d*d));
					}else if(d<100){
						pesti[x].vx+=0.07*(dx/d);
						pesti[x].vy+=0.07*(dy/d);
					}
				}
			}
			var dx = mouse.x - pesti[x].x._;
			var dy = mouse.y - pesti[x].y._;
			
			var d=Math.sqrt(dx*dx+dy*dy);
			if(d<100){
				pesti[x].vx+=1*(-dx/(d));
				pesti[x].vy+=1*(-dy/(d));
			}
		
		pesti[x].move();
		}
	}
}