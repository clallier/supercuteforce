package supercuteforce;

import flambe.System;
import supercuteforce.scene.PreloaderScene;

class Main
{		
    private static function main ()
    {		
        // Wind up all platform-specific stuff

		System.init();
	
		var ctx = new GV();
		trace("version:" + GV.version);
		
		//resize event
		System.stage.resize.connect(GV.resize);

		trace("main: unwindto preloader");
        GV.unwindToScene(new PreloaderScene().getScene());
    }
}
