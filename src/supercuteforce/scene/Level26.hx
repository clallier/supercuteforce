package supercuteforce.scene;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.actions.ScoreCondition;
import supercuteforce.backgrounds.TokyoCityBackground;
import supercuteforce.blocks.BlockFactory.BlockType;
import supercuteforce.gui.Dialog;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level26 extends GameScene
{

	public function new(noDlg:Bool) {
		super("26", 1, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new TokyoCityBackground());
		setVictoryCondition(new ScoreCondition(this, 3000));
		super.initRules();
	}
	
	override function initDlg():Void
	{
		
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(250), true);		
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(251), false);		
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(252), true);		
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(253), true);		
		dialog.addTalk(GV.imgLuckyGod, GV.nameLuckyGod, GV.getText(254), true);		
		
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}

	override public function buildScene() 
	{
		initNewLines(11, 6);
		createAndAddBlock(5, 0, BlockType.snake_head);
		container.add(new Generator(this, 4));
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
	
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(255), false);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(256), true);
		
		dialog.executeAtEnd(buildSecondDlg);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
	
	function buildSecondDlg() 
	{
		shakeScreen(40);
		GV.bPack.getSound("quake").play();
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(257), true);
		dialog.addTalk(GV.imgLuckyGod, GV.nameLuckyGod, GV.getText(258), true);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
}