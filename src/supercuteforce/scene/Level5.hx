package supercuteforce.scene;
import supercuteforce.gui.Dialog;
import flambe.Entity;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.script.Delay;
import flambe.script.CallFunction; 
import supercuteforce.backgrounds.BoidsBackground;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level5 extends GameScene
{

	public function new(noDlg:Bool) 
	{
		super("5", 3, noDlg);
	}
	
	override function initRules():Void {
		hud.setBulletLeft(4);
		setBackground(new BoidsBackground());
		super.initRules();
	}
	
	override function initDlg():Void 
	{
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(43), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(44), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(45), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(46), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(47), true);
		
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() {
		var map = [
		[0, 0, 2, 2, 1, 2, 2, 0, 0],
		[0, 2, 2, 1, 1, 1, 2, 2, 0],
		[0, 0, 1, 2, 2, 2, 1, 0, 0],
		[0, 1, 1, 2, 2, 2, 1, 1, 0],
		[0, 0, 1, 2, 1, 2, 1, 0, 0],
		[0, 0, 0, 1, 1, 1, 0, 0, 0]];
		
		setMap(map);
		
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(48), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(49), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(50), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(51), false);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
}