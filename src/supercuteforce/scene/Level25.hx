package supercuteforce.scene;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.backgrounds.TokyoCityBackground;
import supercuteforce.blocks.BlockFactory.BlockType;
import supercuteforce.gui.Dialog;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level25 extends GameScene
{

	public function new(noDlg:Bool) {
		super("25", 2, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new TokyoCityBackground());
		super.initRules();
	}
	
	override function initDlg():Void
	{
		
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgLuckyGod, GV.nameLuckyGod, GV.getText(240), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(241), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(242), false);
		
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() 
	{
		initNewLines(11, 6);
		createAndAddBlock(5, 0, BlockType.snake_head);
		container.add(new Generator(this, 12));
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(243), false);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(244), true);
		
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
}