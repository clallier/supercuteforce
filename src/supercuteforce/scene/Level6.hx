package supercuteforce.scene;
import supercuteforce.gui.Dialog;
import flambe.Entity;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.script.Delay;
import flambe.script.CallFunction;
import supercuteforce.backgrounds.BoidsBackground;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level6 extends GameScene
{

	public function new(noDlg:Bool) 
	{
		super("6", 3, noDlg);
	}
	
	override function initRules():Void {
		hud.setBulletLeft(5);
		setBackground(new BoidsBackground());
		super.initRules();
	}
	
	override function initDlg():Void 
	{
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(52), false);
		
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() {
		var map = [
		[0, 2, 1, 1, 0, 1, 1, 2, 0],
		[0, 2, 2, 9, 9, 9, 2, 2, 0],
		[0, 1, 2, 0, 9, 0, 2, 1, 0],
		[0, 1, 1, 0, 0, 0, 1, 1, 0],
		[2, 2, 1, 0, 0, 0, 1, 2, 2],
		[0, 2, 2, 0, 0, 0, 2, 2, 0]];
		setMap(map);
		
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(53), false);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(54), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(55), false);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(56), true);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(57), true);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(58), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(59), false);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
}