package supercuteforce.scene;
import flambe.display.Sprite;
import supercuteforce.gui.VerticalMenu;
import supercuteforce.gui.Button;
import flambe.Entity;
import flambe.System;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class ShuffleSelectionScene extends GameScene
{

	public function new() 
	{
		super("inf sel", 0);
		var title:Sprite = new Button("btn_left", "btn_center", "btn_right", GV.getText(335));
		
		var menu:VerticalMenu = new VerticalMenu();
		if(System.storage.get(GV.keyEasy, false) == true)
			menu.addButton(new Button("2_left", "btn_center", "btn_right", GV.getText(336), gotoEasy));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "   ??   "));
		
		if(System.storage.get(GV.keyMedium, false) == true)
			menu.addButton(new Button("3_left", "btn_center", "btn_right", GV.getText(337), gotoMedium));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "   ??   "));
			
		if(System.storage.get(GV.keyHard, false) == true)
			menu.addButton(new Button("4_left", "btn_center", "btn_right", GV.getText(338), gotoHard));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "   ??   "));
		
		if(System.storage.get(GV.keyVHard, false) == true)
			menu.addButton(new Button("4_left", "btn_center", "btn_right", GV.getText(339), gotoVHard));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "   ??   "));
		
			
		menu.addButton(new Button("btn_menu", "btn_center", "btn_right", GV.getText(321), gotoMainMenu));
		
		container.addChild(new Entity().add(title));
		container.addChild(new Entity().add(menu));
		
		title.setXY(mapWidthInPx() / 2, 2 * GV.k);
		menu.setXY(mapWidthInPx() / 2, mapHeightInPx() / 2);
	}
	
	public function gotoEasy()
	{
		scenario.gotoInfinityModeEasy();
	}
	
	public function gotoMedium()
	{
		scenario.gotoInfinityModeMedium();
	}
	
	public function gotoHard()
	{
		scenario.gotoInfinityModeHard();
	}

	public function gotoVHard()
	{
		scenario.gotoInfinityModeVHard();
	}
}