package supercuteforce.scene;

import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.backgrounds.ColorCityBackground;
import supercuteforce.blocks.BubblePink;
import supercuteforce.gui.Dialog;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level12 extends GameScene
{
	public function new(noDlg:Bool) {
		super("12", 3, noDlg);
	}
	
	override function initRules():Void {
		BubblePink.setMuted(true);
		hud.setBulletLeft(9);
		setBackground(new ColorCityBackground());
		super.initRules();
	}
	
	override function initDlg():Void 
	{
		
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(104), false);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(105), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(106), false);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(107), true);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(108), true);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(109), true);
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() 
	{
		var map = [
		[4, 4, 1, 4, 4, 1, 1, 1, 4],
		[4, 4, 1, 1, 4, 1, 4, 4, 4],
		[9, 3, 4, 1, 4, 2, 2, 2, 3],
		[9, 3, 4, 4, 3, 3, 3, 2, 3],
		[9, 3, 3, 4, 2, 3, 9, 9, 3],
		[9, 4, 2, 2, 2, 9, 9, 4, 3],
		[3, 4, 4, 4, 1, 4, 4, 4, 2],
		[3, 3, 3, 1, 1, 1, 2, 2, 2],
		[4, 4, 4, 4, 4, 4, 4, 4, 4]];
		setMap(map);
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(110), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(111), false);
		
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
}