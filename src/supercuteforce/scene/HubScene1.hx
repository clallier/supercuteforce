package supercuteforce.scene;
import flambe.Entity;
import flambe.System;
import supercuteforce.gui.Button;
import supercuteforce.gui.GridMenu;
import supercuteforce.GV;
import flambe.scene.SlideTransition;
import flambe.animation.Ease;


/**
 * ...
 * @author Godjam - QilinEggs
 */
class HubScene1 extends GameScene
{
	public function new ()
	{
		
		super("hub1", 0);
		var access = System.storage.get(GV.keyLevel, 1);
		var menu = new GridMenu(this);
		
		if(access >= 1)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "01", gotoLevel01));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
			
		if(access >= 2)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "02", gotoLevel02));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
		
		if(access >= 3)	
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "03", gotoLevel03));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
		
		if(access >= 4)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "04", gotoLevel04));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));

		if(access >= 5)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "05", gotoLevel05));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));

		if(access >= 6)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "06", gotoLevel06));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));

		if(access >= 7)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "07", gotoLevel07));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
			
		if (access >= 8)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "08", gotoLevel08));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));

		if(access >= 9)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "09", gotoLevel09));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));

		if(access > 9)
			menu.setNextButton(new Button("hud_next", "btn_center", "btn_right", GV.getText(326), gotoNext));
		
	
		container.addChild(new Entity().add(menu));
		
		menu.setXY(	mapWidthInPx()/2 - menu.getNaturalWidth()/2+GV.k/2, 
					mapHeightInPx() / 2 - menu.getNaturalHeight() / 2);
		
	}
	
	function gotoLevel01() { scenario.gotoScene(1); }
	function gotoLevel02() { scenario.gotoScene(2); }
	function gotoLevel03() { scenario.gotoScene(3); }
	function gotoLevel04() { scenario.gotoScene(4); }
	function gotoLevel05() { scenario.gotoScene(5); }
	function gotoLevel06() { scenario.gotoScene(6); }
	function gotoLevel07() { scenario.gotoScene(7); }
	function gotoLevel08() { scenario.gotoScene(8); }
	function gotoLevel09() { scenario.gotoScene(9); }
	
	function gotoNext() {
		var scene = new HubScene2();
		scene.setScenario(scenario);
		var transition = new SlideTransition(1, Ease.quintInOut).up();
		GV.unwindToScene(scene.getScene(), transition); 
	}
		
}