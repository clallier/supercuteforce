package supercuteforce.scene;
import flambe.Entity;
import flambe.System;
import supercuteforce.gui.Button;
import supercuteforce.gui.GridMenu;
import supercuteforce.GV;
import flambe.scene.SlideTransition;
import flambe.animation.Ease;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class HubScene2 extends GameScene
{
	
	public function new ()
	{	
		super("hub2", 0);
		var access = System.storage.get(GV.keyLevel, 1);
		var menu = new GridMenu(this);
		if(access >= 10)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "10", gotoLevel10));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
		
		if(access >= 11)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "11", gotoLevel11));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
		
		if(access >= 12)
		menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "12", gotoLevel12));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
		
		if(access >= 13)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "13", gotoLevel13));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
		
		if(access >= 14)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "14", gotoLevel14));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
		
		if(access >= 15)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "15", gotoLevel15));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
		
		if(access >= 16)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "16", gotoLevel16));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
		
		if(access >= 17)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "17", gotoLevel17));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
		
		if(access >= 18)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "18", gotoLevel18));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
			
		// previous
		menu.setPrevButton(new Button("hud_prev", "btn_center", "btn_right", GV.getText(327), gotoPrev));
		
		// next
		if(access > 18)
			menu.setNextButton(new Button("hud_next", "btn_center", "btn_right", GV.getText(326), gotoNext));
		
		container.addChild(new Entity().add(menu));
		
		menu.setXY(	mapWidthInPx()/2 - menu.getNaturalWidth()/2+GV.k/2, 
					mapHeightInPx()/2 - menu.getNaturalHeight()/2);
		
	}
	
	function gotoLevel10() { scenario.gotoScene(10); }
	function gotoLevel11() { scenario.gotoScene(11); }
	function gotoLevel12() { scenario.gotoScene(12); }
	function gotoLevel13() { scenario.gotoScene(13); }
	function gotoLevel14() { scenario.gotoScene(14); }
	function gotoLevel15() { scenario.gotoScene(15); }
	function gotoLevel16() { scenario.gotoScene(16); }
	function gotoLevel17() { scenario.gotoScene(17); }
	function gotoLevel18() { scenario.gotoScene(18); }
	
	function gotoPrev() {
		var scene = new HubScene1();
		scene.setScenario(scenario);
		var transition = new SlideTransition(1, Ease.quintInOut).down();
		GV.unwindToScene(scene.getScene(), transition); 
	}
	
	function gotoNext() {
		var scene = new HubScene3();
		scene.setScenario(scenario);
		var transition = new SlideTransition(1, Ease.quintInOut).up();
		GV.unwindToScene(scene.getScene(), transition); 
	}
		
	
}