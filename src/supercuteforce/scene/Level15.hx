package supercuteforce.scene;

import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.backgrounds.ColorCityBackground;
import supercuteforce.blocks.BubblePink;
import supercuteforce.gui.Dialog;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level15 extends GameScene
{

	public function new(noDlg:Bool) {
		super("15", 3, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new ColorCityBackground());
		BubblePink.setMuted(true);
		hud.setBulletLeft(8);
		super.initRules();
	}
	
	override function initDlg():Void
	{
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(126), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(127), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(128), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(129), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(130), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(131), true);	
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(132), true);
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() 
	{
		var map = [
		[3, 4, 3, 3, 4, 3, 3, 4, 3],
		[1, 4 ,1, 1, 4 ,1, 1, 4 ,1],
		[2, 2, 2, 2, 2, 1, 1, 2, 1],
		[1, 1, 1, 1, 5, 1, 3, 3, 2],
		[1, 2, 2, 2, 4, 4, 2, 3, 2],
		[1, 5, 4, 4, 9, 4, 4, 5, 2],
		[9, 1, 3, 3, 1, 3, 3, 1, 9],
		[1, 3, 1, 1, 1, 1, 1, 2, 1],
		[3, 3, 3, 3, 5, 2, 2, 2, 2]];
		setMap(map);
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(133), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(134), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(135), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(136), false);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
}