package supercuteforce.scene;

import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.backgrounds.ColorCityBackground;
import supercuteforce.gui.Dialog;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level18 extends GameScene
{
		
	public function new(noDlg:Bool) {
		super("18", 3, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new ColorCityBackground());
		hud.setBulletLeft(9);
		super.initRules();
	}
	
	override function initDlg():Void 
	{
		
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(160), false);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(161), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(162), false);
		dialog.addTalk(GV.imgTranspOn, GV.nameTranspon, GV.getText(163), false);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(164), true);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(165), true);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(166), true);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(167), true);
		
		dialog.executeAtEnd(buildSecondDialog);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	function buildSecondDialog() 
	{
		shakeScreen(40);
		GV.bPack.getSound("quake").play();
		var dialog:Dialog = new Dialog();
		
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(168), false);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(169), true);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(170), true);
	
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
	
	override public function buildScene() 
	{
		var map = [
		[4, 2, 2, 1, 1, 1, 2, 2, 4],
		[4, 4 ,2, 2, 1 ,2, 2, 4 ,4],
		[4, 3, 3, 3, 3, 3, 3, 3, 4],
		[2, 1, 1, 3, 5, 3, 1, 1, 2],
		[2, 1, 1, 3, 5, 3, 1, 1, 2],
		[2, 2, 4, 4, 5, 4, 4, 2, 2],
		[3, 4, 4, 1, 4, 1, 4, 4, 3],
		[3, 4, 1, 1, 3, 1, 1, 4, 3],
		[3, 3, 1, 3, 3, 3, 1, 3, 3]];
		setMap(map);
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(171), false);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(172), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(173), false);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(174), true);
		dialog.executeAtEnd(buildSecondEnd);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
	function buildSecondEnd() 
	{
		shakeScreen(40);
		GV.bPack.getSound("quake").play();
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(175), false);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(176), false);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
	override function runEndWithLose() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(177), false);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(178), false);
		dialog.executeAtEnd(showLoseMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
}