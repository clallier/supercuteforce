package supercuteforce.scene;

import flambe.display.ImageSprite;
import flambe.display.Sprite;
import flambe.Entity;
import flambe.script.AnimateBy;
import flambe.script.AnimateTo;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Parallel;
import flambe.script.Repeat;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.System;
import flambe.animation.Ease;
import supercuteforce.gui.Button;
import supercuteforce.gui.MagicText;
import supercuteforce.gui.SoundButton;
import supercuteforce.gui.VerticalMenu;
import supercuteforce.GV;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class IntroScene extends GameScene
{
	public function new ()
	{
		super("intro", 0);
		trace("intro scene loaded");
		var logo:Sprite;
		var script:Script = new Script();
		var logo_bg:Sprite = new ImageSprite(GV.tPack.getTexture("logo_bg"));	
		
		if(System.storage.get(GV.keyVHard, false) == false)
			logo = new ImageSprite(GV.tPack.getTexture("logo"));	
		else 
			logo = new ImageSprite(GV.tPack.getTexture("logo2"));
			
		var menu:VerticalMenu = new VerticalMenu();
		//menu.addButton(new Button("btn_left", "btn_center", "btn_right", GV.getText(360), gotoHowToPlay));
		menu.addButton(new Button("btn_left", "btn_center", "btn_right", GV.getText(361), gotoStoryMode));
		menu.addButton(new Button("btn_left", "btn_center", "btn_right", GV.getText(335), gotoShuffleMenu));
		//menu.addButton(new Button("btn_left", "btn_center", "btn_right", GV.getText(352), requestFS));
		menu.addButton(new SoundButton("btn_left", "btn_center", "btn_right", GV.soundSwitch));
		menu.addButton(new MagicText(gotoQilinEggs));
		
		container.addChild(new Entity().add(logo_bg).add(script));
		container.addChild(new Entity().add(logo));
		container.addChild(new Entity().add(menu));
		
		logo_bg.centerAnchor();
		logo_bg.setXY(mapWidthInPx()/2, -6 * GV.k);
		logo_bg.alpha._ = 1;
		logo.centerAnchor();
		logo.setXY(mapWidthInPx()/2, -6 * GV.k);
		menu.setXY(mapWidthInPx()/2, GV.height);
		script.run(new Sequence([
					new CallFunction(playSuperCuteForce),
					// delay
					new Delay(1),
					new Parallel([
						// pop logo and logo_bg
						new AnimateTo(logo_bg.y, 4 * GV.k, 0.6, Ease.bounceOut),
						new AnimateTo(logo.y, 4 * GV.k, 0.5, Ease.bounceOut),
						// pop menu 
						new AnimateTo(menu.y, 8 * GV.k, 0.6, Ease.bounceOut)]),
					// repeat rotation
					new Repeat(new AnimateBy(logo_bg.rotation, 360, 16))]));
	}
	
	function playSuperCuteForce() 
	{
		GV.bPack.getSound("supercuteforce").play();
	}

	public function gotoStoryMode()
	{
		scenario.gotoFirstScene();
	}
	
	public function gotoHowToPlay()
	{
		scenario.gotoHowToPlay();
	}
	
	public function requestFS()
	{
		System.stage.requestFullscreen(true);
	}
	
	public function gotoQilinEggs()
	{
		System.web.openBrowser("http://www.qilineggs.com");
	}
}