package supercuteforce.scene;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.actions.ScoreCondition;
import supercuteforce.backgrounds.TokyoCityBackground;
import supercuteforce.gui.Dialog;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level20 extends GameScene
{
		
	public function new(noDlg:Bool) {
		super("20", 1, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new TokyoCityBackground());
		setVictoryCondition(new ScoreCondition(this, 2000));
		super.initRules();
	}
	
	override function initDlg():Void
	{
				
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(187), false);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(188), false);
		
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() 
	{
		initNewLines(10, 5);
		container.add(new Generator(this, 5));
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(189), false);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(190), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(191), false);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(192), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(193), false);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(194), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(195), true);
		
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
}