package supercuteforce.scene;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.gui.ControlMenu;
import supercuteforce.gui.Dialog;
import supercuteforce.gui.InterSceneMenu;
import flambe.scene.Director;
import flambe.scene.FadeTransition;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class ShuffleGameScene extends GameScene
{
	override function initDlg():Void {
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(280), true);
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override function gotoInterSceneMenu(victory:Bool) {
		if (victory)
			scenario.validateWinLevel();
		
		var menu = new InterSceneMenu(this, victory, true);
		var d:Director = GV.getRoot().get(Director);
		if (d != null) {
			d.pushScene(new Entity().add(menu), new FadeTransition(0.2));
		}
	}
	
	override public function gotoPauseMenu() {	
		pause();
		setActive(false);
		var menu = new ControlMenu(this, false, true);
		var d:Director = GV.getRoot().get(Director);
		if (d != null) {
			d.pushScene(new Entity().add(menu), new FadeTransition(0.2));
		}
	}
}