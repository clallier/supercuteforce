package supercuteforce.scene;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.backgrounds.TokyoCityBackground;
import supercuteforce.blocks.BubblePink;
import supercuteforce.gui.Dialog;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level21 extends GameScene
{
public function new(noDlg:Bool) {
		super("21", 3, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new TokyoCityBackground());
		BubblePink.setMuted(false);
		hud.setBulletLeft(4);
		super.initRules();
	}
	
	override function initDlg():Void
	{
				
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(196), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(197), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(198), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(199), false);
		
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() 
	{
		var map = [
		[1, 4, 1, 4, 1, 4, 1, 4, 1],
		[1, 4, 1, 4, 1, 4, 1, 4, 1],
		[1, 4, 1, 4, 1, 4, 1, 4, 1],
		[1, 4, 1, 4, 1, 4, 1, 4, 1],
		[9, 6, 9, 9, 6, 9, 9, 6, 9],
		[2, 1, 2, 3, 1, 3, 2, 1, 2],
		[2, 1, 2, 3, 1, 3, 2, 1, 2]];
		setMap(map);
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(200), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(201), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(202), false);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(203), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(204), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(205), false);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(206), true);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
}