package supercuteforce.scene;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.backgrounds.TokyoCityBackground;
import supercuteforce.gui.Dialog;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level24 extends GameScene
{

public function new(noDlg:Bool) {
		super("24", 3, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new TokyoCityBackground());
		hud.setBulletLeft(11);
		super.initRules();
	}
	
	override function initDlg():Void
	{
	
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(230), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(231), false);
		dialog.addTalk(GV.imgLuckyGod, GV.nameLuckyGod, GV.getText(232), true);
		dialog.addTalk(GV.imgLuckyGod, GV.nameLuckyGod, GV.getText(233), true);
		
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() 
	{
		var map = [
		[1, 2, 1, 1, 7, 1, 1, 2, 1],
		[4, 4 ,4, 1, 1 ,1, 4, 4 ,4],
		[2, 4, 3, 3, 3, 3, 3, 4, 2],
		[2, 2, 9, 1, 6, 1, 9, 2, 2],
		[2, 6, 1, 1, 5, 1, 1, 6, 2],
		[2, 5, 1, 5, 5, 5, 1, 5, 2],
		[9, 2, 9, 1, 9, 1, 9, 2, 9],
		[2, 2, 2, 1, 1, 1, 2, 2, 2],
		[3, 3, 3, 3, 1, 3, 3, 3, 3]];
		setMap(map);
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(234), false);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(235), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(236), true);
		dialog.addTalk(GV.imgLuckyGod, GV.nameLuckyGod, GV.getText(237), true);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}

}