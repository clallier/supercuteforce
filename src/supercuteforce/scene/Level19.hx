package supercuteforce.scene;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.System;
import supercuteforce.backgrounds.TokyoCityBackground;
import supercuteforce.gui.Dialog;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level19 extends GameScene
{
		
	public function new(noDlg:Bool) {
		super("19", 2, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new TokyoCityBackground());
		System.storage.set(GV.keyMedium, true);
		super.initRules();
	}
	
	override function initDlg():Void
	{
		
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgTheGifts, GV.nameNewItems, GV.getText(180), false);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(181), false);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(182), true);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(183), true);
		
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() 
	{
		initNewLines(10, 5);
		container.add(new Generator(this, 13));
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(184), false);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(185), false);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
}