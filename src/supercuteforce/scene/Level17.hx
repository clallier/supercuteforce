package supercuteforce.scene;

import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.actions.ScoreCondition;
import supercuteforce.backgrounds.ColorCityBackground;
import supercuteforce.blocks.BubblePink;
import supercuteforce.gui.Dialog;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level17 extends GameScene
{

	
	public function new(noDlg:Bool) {
		super("17", 1, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new ColorCityBackground());
		setVictoryCondition(new ScoreCondition(this, 2000));
		BubblePink.setMuted(true);
		super.initRules();
	}
	
	override function initDlg():Void
	{
		
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(150), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(151), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(152), true);
		dialog.addTalk(GV.imgTransOff, GV.nameNewItems, GV.getText(153), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(154), true);
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() 
	{
		initNewLines(9, 5);
		container.add(new Generator(this, 5));
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(155), false);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(156), false);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
}