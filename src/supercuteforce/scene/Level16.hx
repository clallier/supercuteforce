package supercuteforce.scene;

import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.backgrounds.ColorCityBackground;
import supercuteforce.blocks.BubblePink;
import supercuteforce.gui.Dialog;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level16 extends GameScene
{

	
	public function new(noDlg:Bool) {
		super("16", 2, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new ColorCityBackground());
		BubblePink.setMuted(true);
		super.initRules();
	}
	
	override function initDlg():Void
	{
		
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(137), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(138), false);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(139), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(140), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(141), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(142), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(143), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(144), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(145), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(146), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(147), true);
		
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() 
	{
		initNewLines(9, 5);
		container.add(new Generator(this, 14));
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(148), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(149), false);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
}