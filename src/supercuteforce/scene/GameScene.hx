package supercuteforce.scene;

import flambe.animation.Ease;
import flambe.display.EmitterSprite;
import flambe.display.Sprite;
import flambe.display.TextSprite;
import flambe.Entity;
import flambe.math.Point;
import flambe.scene.Director;
import flambe.scene.FadeTransition;
import flambe.scene.Scene;
import flambe.script.Action;
import flambe.script.AnimateTo;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.MoveTo;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.script.Shake;
import flambe.SpeedAdjuster;
import supercuteforce.actions.EmptyMapCondition;
import supercuteforce.actions.Gravity;
import supercuteforce.actions.NoMoreBulletCondition;
import supercuteforce.actions.SwapBuffer;
import supercuteforce.backgrounds.Background;
import supercuteforce.backgrounds.BoidsBackground;
import supercuteforce.backgrounds.VoidBackground;
import supercuteforce.blocks.Block;
import supercuteforce.blocks.BlockFactory;
import supercuteforce.blocks.BubblePink;
import supercuteforce.Bullet;
import supercuteforce.gui.ControlMenu;
import supercuteforce.gui.Hud;
import supercuteforce.gui.InterSceneMenu;
import supercuteforce.gui.Scenario;
import supercuteforce.MusicPlayer;
import supercuteforce.Player;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class GameScene extends Scene
{
	var scene:Entity;
	var container:Entity;
	
	var map:Array<Array<Block>>;
	var buffermap:Array<Array<Block>>;
	var hud:Hud;
	var player:Player;
	var scenario:Scenario;
	var background:Background;
	private var active:Bool;
	private var movingObjects:Array<Sprite>; //if it return to 0 => new "turn"
	var actions:Map<String, Action>;
	var victoryCondition:Action;
	var loseCondition:Action;
	
	private var oldMovingObjCount:Int = 0;
	private var oldScore:Int = 0;
	public var orig:Point;
	public static var instances:Int = 0;
	public static var musicPlayer:MusicPlayer = new MusicPlayer();

	
	public function new(levelName:String, idMusic:Int, noDlg:Bool = false)
	{	
		++instances;
		//trace("instances : " + instances);
		
		super();
		BubblePink.setMuted(false);
		movingObjects = new Array<Sprite>();
		actions = new Map<String, Action>();
		victoryCondition = new EmptyMapCondition(this);
		loseCondition = new NoMoreBulletCondition(this);
		// scene 
		scene = new Entity();
		
		//music player
		scene.addChild(new Entity().add(musicPlayer));
		musicPlayer.startMusic();
		
		// bg 
		setBackground(new VoidBackground());
		
		//scene
		scene.addChild(new Entity().add(this));
		
		// container 
		initContainer();
		
		// map
		initMap();
		
		//hud
		hud = new Hud(this,levelName);
		container.addChild(new Entity().add(hud));
		hud.setXY(-orig.x, -orig.y);

		// player
		player = new Player(this);
		player.setXY(mapWidthInPx() - GV.k, mapHeightInPx() - GV.k*2);
		container.addChild(new Entity().add(player));
			
		// default state
		setActive(false);
		
		// get specific rules
		initRules();
		
		// init dialogs
		if (noDlg)initNoDlg();
		else initDlg();
	}
	
	override public function dispose() {
		super.dispose();
		//musicPlayer.dispose();
		--instances;
		//trace("instances : " + instances);
	}
	
	function initRules():Void{}
	function initDlg():Void{}
	
	private function initNoDlg():Void 
	{
		var script = container.get(Script);
			if (script != null)
			script.run(new Sequence([	new Delay(0.8), 				
				new CallFunction(buildScene)]));
	}
	
	public function getScene() :Entity 
	{
		return scene; 
	}
	
	
	public function getMap() 
	{
		return map;
	}
	
		public function getPlayer() 
	{
		return player;	
	}
	
	public function getHud() 
	{
		return hud;
	}
	
	/********************************************************
	 *  Map functionnality
	 ********************************************************/
	
	public function swapMap() 
	{
		this.map = this.buffermap.copy();
		//trace("swap");
	}
	
	public function setMap(levelmap:Array<Array<Int>>) 
	{
		for (y in 0...levelmap.length) {
			for (x in 0...levelmap[y].length) {
				createAndAddBlock(x, y, levelmap[y][x]);
			}
		}
		
		swapMap();
	}
	public function initNewLines(lines:Int = 6, max:Int=6) 
	{
		GV.max_types = max;
		for (y in 0...GV.mapHeight) {
			for (x in 0...GV.mapWidth) {
				if(y < lines) {
					createAndAddRandomBlock(x, y, max);
				}
			}
		}
		swapMap();
	}

	public function createAndAddBlock(xTile:Int, yTile:Int, type:Int, xFrom:Int = -1, yFrom = -1):Void 
	{
		var block = BlockFactory.create(this, xTile, yTile, type);
		addBlock(xTile, yTile, block, xFrom, yFrom);
	}
	
	public function createAndAddGreenOrBlueBlock(xTile:Int, yTile:Int, xFrom:Int, yFrom:Int):Void 
	{
		var block = BlockFactory.createRandomBlock(this, xTile, yTile, BlockType.bubble_green);
		addBlock(xTile, yTile, block, xFrom, yFrom);
	}
	
	public function createAndAddRandomBlock(xTile:Int, yTile:Int, max:Int = -1):Void 
	{
		if (max == -1) max = GV.max_types;
		var block = BlockFactory.createRandomBlock(this, xTile, yTile, max);
		addBlock(xTile, yTile, block);
	}
	
	public function createAndAddExistingBlock(xTile:Int, yTile:Int, existingTypes:Array<Int>) 
	{
		if (existingTypes.length == 0)
			for (i in 0...GV.max_types)
				existingTypes.push(i);
		
		var t = existingTypes[Std.int(Math.random() * existingTypes.length)];
		createAndAddBlock(xTile, yTile, t);
	}
		
	public function addBlock(xTile:Int, yTile:Int, block:Block, xFrom:Int=-1, yFrom:Int=-1) 
	{
		if (block == null) return;
		if (xTile < 0 || xTile >= GV.mapWidth 
		|| yTile < 0 || yTile >= GV.mapHeight)
			return;
			
		if (buffermap[yTile][xTile] != null) {
			//trace("add Block : not empty");
			deleteBlock(getBlock(xTile, yTile));
		}
			
		buffermap[yTile][xTile] = block;
		container.addChild(new Entity().add(buffermap[yTile][xTile]), false);
		block.moveTo(xFrom, yFrom, xTile, yTile, Math.random());
	}
	
	public function hasBlock() 
	{
		for (y in 0...GV.mapHeight) {
			for (x in 0...GV.mapWidth) {
				if (this.map[y][x] != null)
					return true;
			}
		}	
		return false;
	}
	
	public function getBlock(tileX:Int, tileY:Int) :Block
	{
		if ( tileX >= 0 && tileX < GV.mapWidth && 
			tileY >= 0 && tileY < GV.mapHeight)
			return map[tileY][tileX];
		else return null;
	}
	
	public function removeBlock(xTile:Int, yTile:Int) 
	{
		var block = getBlock(xTile, yTile);
		
		if (block != null) {
			block.owner.dispose();
			
			buffermap[yTile][xTile] = null;
			return block;
		}
		
		return null;
	}
	
	public function deleteBlock(block:Block) 
	{
		if (block == null)
			return;
		
		var x = block.xTile;
		var y = block.yTile;
		if(block.owner != null)
			block.owner.dispose();
		else { 
			trace("ERROR : OWNER NULL");
			block.visible = false;
		}
		buffermap[y][x] = null;
	}
	
	public function cutPasteBlock(xFrom:Int, yFrom:Int, xTo:Int, yTo:Int) 
	{
		
		var copy = getBlock(xFrom, yFrom).copy(xTo, yTo);
		addBlock(xTo, yTo, copy, xFrom, yFrom);
		deleteBlock(getBlock(xFrom, yFrom));
		/*
		var copy = getBlock(xFrom, yFrom).copy();
		addBlock(xTo, yTo, copy);
		deleteBlock(getBlock(xFrom, yFrom));
		*/
	}
	
	public function detachBlock(block:Block, g:Int) 
	{
		var x = block.xTile;
		var y = block.yTile;
		buffermap[y][x] = null;
		
		if (block.owner != null)
			block.owner.dispose();
		block.owner = null;
		
		addMovingObject(block, new Sequence([new AnimateTo(block.y, mapHeightInPx()-GV.k*0.5, 2, Ease.bounceOut), new CallFunction(block.disposeAsMovingObject)]));
		block.alpha.animateTo(0.2, 1.5);		
		if (block.type == BlockType.rock)
			g = 2;
		
		incrementScore(g);
		addScoreText(x, y, g);
	}
	

	public function addMovingObject(s:Sprite, a:Action = null) :Void 
	{
		movingObjects.remove(s);
		var e = new Entity();
		
		// script creation 
		if (a != null)
			e.add(new Script());
		
		container.addChild(e.add(s));
		movingObjects.push(s);
		
		// begin to run script
		if(a != null) {
			var script = e.get(Script);
			if (script != null) script.run(a);
		}
	}
	
	public function deleteMovingObject(s:Sprite):Void 
	{
		if(s.owner != null)
			s.owner.dispose();
		s.owner = null;

		movingObjects.remove(s);
		s = null;
	}
	
	override public function onUpdate(dt:Float) 
	{
		super.onUpdate(dt);
		checkCompleteEOT(oldMovingObjCount, movingObjects.length);
	}
	
	/**
	 * Value triggered EOT, based of the numbre of moving objects
	 * @param	oldVal
	 * @param	newVal
	 */
	function checkCompleteEOT(oldVal:Int, newVal:Int) 
	{
		var modified = 0;
		
		// no more object moving
		if(oldVal != newVal)
		//trace("moving objects : ", oldVal, newVal);
		
		if (newVal == 0 && oldVal != 0 && isActive() ) {
			var g:Gravity = new Gravity(this);
			var s:Script = container.get(Script);
			s.run(g);
			modified = g.hasModifiedMap();
			
			//trace("gravity modified : " + modified);
		}
		
		// is there any moving objects ?
		//     Yes => okay init new cycle then return 
		if (modified == 0 && newVal == 0 && oldVal != 0 && isActive() ) {
			comboWatcher(hud.getScore());
			var s:Script = container.get(Script);
			s.run(new Sequence([victoryCondition, loseCondition, 
								new CallFunction(runActionsBuffer),
								new CallFunction(resetShoot),
								new SwapBuffer(this)]));
		}	
		oldMovingObjCount = movingObjects.length;
	}
	
	function resetShoot() 
	{
		player.resetShoot();
		//trace("reset shoot");
	}
	
	public function clearActionsQueue() 
	{
		actions = new Map<String, Action>();
	}
	
	private function runActionsBuffer() 
	{
		var s:Script = container.get(Script);
			var seqBuffer = new Sequence();
			
			var i = actions.iterator();
			while(i.hasNext())
				seqBuffer.add(i.next());
			clearActionsQueue();
			s.run(seqBuffer);
	}
		
	public function addBullet(xTile:Int, yTile:Int, angle:Int=0, type:Int=0, g:Int=1) 
	{
		addMovingObject(new Bullet(this, xTile * GV.k + GV.k / 2, yTile * GV.k + GV.k / 2, angle, type, g));
	}
	
	public function addLoot(loot:Int, xTile:Int, yTile:Int) 
	{
		addMovingObject(BlockFactory.createLoot(this, xTile, yTile, loot));
	}
	
	public function addEmitter(x:Float, y:Float) 
	{
		var emitter = new EmitterSprite(GV.mold);
		emitter.emitX._ = x;
		emitter.emitY._ = y;
		emitter.restart();
		var e = new Entity().add(emitter).add(new Script());
		var scr = e.get(Script);
		if( scr != null)
			scr.run(new Sequence([new Delay(0.4), new CallFunction(e.dispose)]));
		
		container.addChild(e);
	}
	public function addSmallEmitter(x:Float, y:Float) 
	{
		var emitter = new EmitterSprite(GV.smallMold);
		emitter.emitX._ = x;
		emitter.emitY._ = y;
		emitter.restart();
		var e = new Entity().add(emitter).add(new Script());
		var scr = e.get(Script);
		if( scr != null)
			scr.run(new Sequence([new Delay(0.1), new CallFunction(e.dispose)]));
		
		container.addChild(e);
	}

	public function incrementScore(score :Int) 
	{
		hud.incrementScore(score);
	}
	
	public function addScoreText(xTile:Int, yTile:Int, score:Int):Void 
	{
		var e = new Entity().add(new TextSprite(GV.font, "+" + score).setXY(xTile * GV.k, yTile * GV.k)).add(new Script());
		var scr = e.get(Script);
		if( scr != null)
			scr.run(new Sequence([new Delay(0.4), new CallFunction(e.dispose)]));
		
		container.addChild(e);
	}
	
	public function shakeScreen(g:Int):Void 
	{
		var scr = container.get(Script);
		
		if( scr != null) {
			var ttshake = g * 0.1;
			var power = GV.k / 16;
			scr.run(new Sequence([new Shake(power, power, ttshake), new Delay(ttshake), new MoveTo(orig.x, orig.y, 0.2)]));
		}
	}
	
	public function mapWidthInPx() 
	{
		return GV.mapWidth * GV.k;
	}
	
	public function mapHeightInPx() 
	{
		return GV.mapHeight* GV.k;
	}
	
	public function pause() {
		var speedAdjuster: SpeedAdjuster = container.get(SpeedAdjuster);
		if (speedAdjuster != null)
			speedAdjuster.scale._ = 0;
	}
	
	public function play() {
		var speedAdjuster: SpeedAdjuster = container.get(SpeedAdjuster);
		if (speedAdjuster != null)
			speedAdjuster.scale._ = 1;
		hud.desactivatePause();
		setActive(true);
	}
	
	public function playSlowly() {
		var speedAdjuster: SpeedAdjuster = container.get(SpeedAdjuster);
		if (speedAdjuster != null)
			speedAdjuster.scale._ = 0.2;
	}

	public function setScenario(scenario:Scenario) 
	{
		this.scenario = scenario;
	}
	
	public function gotoNextScene() {
		hud.validateScore();
		scenario.gotoNextScene();
	}
	
	public function gotoHub() {
		scenario.gotoHub();
	}
	
	public function gotoMainMenu() {
		scenario.gotoMainMenu();
	}
	
	public function gotoRetry() {
		scenario.gotoRetry();
	}
	
	public function gotoFastRetry() {
		scenario.gotoFastRetry();
	}
	
	public function gotoShuffleMenu() 
	{
		scenario.gotoShuffleMenu();
	}
	
	public function gotoPauseMenu() {	
		pause();
		setActive(false);
		var menu = new ControlMenu(this, true, false);
		var d:Director = GV.getRoot().get(Director);
		if (d != null) {
			d.pushScene(new Entity().add(menu), new FadeTransition(0.2));
		}
	}
	
	function gotoInterSceneMenu(victory:Bool) {
		if (victory)
			scenario.validateWinLevel();
		
		var menu = new InterSceneMenu(this, victory);
		var d:Director = GV.getRoot().get(Director);
		if (d != null) {
			d.pushScene(new Entity().add(menu), new FadeTransition(0.2));
		}
	}
	
	public function showTextElement(txt:String) {
		var text:TextSprite = new TextSprite(GV.font, txt);
		text.setXY(-text.getNaturalWidth(), 
					GV.height / 2 - text.getNaturalHeight() / 2);
					
		
		var e:Entity = new Entity();
		e.add(text);
		e.add(new Script());
		GV.getRoot().addChild(e);
		
		var s = e.get(Script);
		
		if( s != null)
		s.run(new Sequence([
			new MoveTo(GV.width / 2 - text.getNaturalWidth() / 2, text.y._, 0.3), 
			new Delay(1), 
			new MoveTo(GV.width, text.y._, 0.3), 
			new CallFunction(e.dispose)]));
	}
		
	/**
	 * Override this to add a end of scene dialog 
	 * default behavior : show Victory Message And Go To Next Scene
	 */
	public function runEndWithVictory() 
	{
		setActive(false);
		showVictoryMessageAndGotoNext();
	}
	
	/**
	 * Override this to add a end of scene dialog 
	 * default behavior : show Lose Message And Go To Next Scene
	 */
	public function runEndWithLose() 
	{
		setActive(false);
		showLoseMessageAndGotoNext();
	}
	
	function showVictoryMessageAndGotoNext():Void 
	{
		gotoInterSceneMenu(true);
	}
	
	function showLoseMessageAndGotoNext():Void 
	{
		gotoInterSceneMenu(false);
	}
	
	/**
	 * Override this to build the scene
	 */
	public function buildScene() {
		//trace("started!");
		setActive(true);
	}
	
	function setActive(bool:Bool) {
   		active = bool;
		hud.visible = bool;
	}
	
	/*
	 * Difference with isInPause
	 * - during dialogs scene is inactive => player can move but cannot shoot
	 * - during pause nothing works except the action menu => player cannot move nor shoot
	 */
	public function isActive() :Bool
	{
		//trace("is active:" + active);
		return active;
	}
		
	public function addAction(action:Action) 
	{
		var type =  Type.getClassName(Type.getClass(action));
		
		if(isActive())
			actions.set(type, action);
	}
	
	public function slomoEffect() 
	{
		var speedAdjuster: SpeedAdjuster = container.get(SpeedAdjuster);
		
		
		if (speedAdjuster != null) {
			speedAdjuster.scale._ = 0; // stop time
			speedAdjuster.scale.animateTo(1, 0.2, Ease.cubeIn); // play slowly
		}
		background.glitch();
	}
	
	public function killPlayer() 
	{
		addEmitter(player.x._, player.y._);
		shakeScreen(20);
		player.visible = false;
		var s = new Script();
		container.addChild(new Entity().add(s));
		s.run(new Sequence([new Delay(2), new CallFunction(runEndWithLose)]));
	}
	
	function comboWatcher(newScore:Int) 
	{
		var d = newScore - oldScore;
		
		// change music 
		if (d >= 128)
			musicPlayer.powerUp();
			
		if (Math.random() < 0.4) return; // 60% of realization
			
		if (d >= 512) 
			showTextElement(GV.getText(355));
		else if(d >= 256) 
			showTextElement(GV.getText(356));
		else if(d >= 128) 
			showTextElement(GV.getText(357));
		else if(d >= 64) 
			showTextElement(GV.getText(358));
		else if(d > 32) 
			showTextElement(GV.getText(359));
			
		oldScore = newScore;
	}
	
	function initMap():Void 
	{
		map =  new Array<Array<Block>>();
		map = [];
		buffermap =  new Array<Array<Block>>();
		buffermap = [];
		
		for (y in 0...GV.mapHeight) {
			map[y] = [];
			buffermap[y] = [];
			for (x in 0...GV.mapWidth) {
				map[y][x] = null;
				buffermap[y][x] = null;
			}	
		}
	}
	
	function setBackground(bg:Background):Void 
	{
		if (background != null) {
			background.owner.dispose();
			background = null;
		}
		
		this.background = bg;
		scene.addChild(new Entity().add(background), false);

	}
	
	function setVictoryCondition(cd:Action) {
		victoryCondition = cd;
	}
	
	function initContainer():Void 
	{
		var width = GV.k * GV.mapWidth; 
		var height = GV.k * GV.mapHeight;
		
		var ox = Std.int(0.5 * GV.width - 0.5 * width);
		var oy = Std.int(0.5 * GV.height - 0.5 * height);
		//trace("centering container : " + ox +", "+ oy);
		orig = new Point(ox, oy);
		container = new Entity();
		container.add(new Sprite().setXY(orig.x, orig.y));
		container.add(new Script());
		container.add(new SpeedAdjuster());
		scene.addChild(container);
	}
	
	public function playMusic(id: Int) {
		musicPlayer.setNextSeq(id);
	}
}