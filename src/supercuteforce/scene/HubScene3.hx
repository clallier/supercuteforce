package supercuteforce.scene;
import flambe.Entity;
import flambe.System;
import supercuteforce.gui.Button;
import supercuteforce.gui.GridMenu;
import supercuteforce.GV;
import flambe.scene.SlideTransition;
import flambe.animation.Ease;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class HubScene3 extends GameScene
{

	public function new ()
	{	
		super("hub3", 0);
		var access = System.storage.get(GV.keyLevel, 1);
		var menu = new GridMenu(this);
		
		if(access >= 19)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "19", gotoLevel19));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
			
		if(access >= 20)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "20", gotoLevel20));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
			
		if(access >= 21)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "21", gotoLevel21));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
			
		if(access >= 22)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "22", gotoLevel22));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
			
		if(access >= 23)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "23", gotoLevel23));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
			
		if(access >= 24)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "24", gotoLevel24));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
			
		if(access >= 25)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "25", gotoLevel25));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
			
		if(access >= 26)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "26", gotoLevel26));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
			
		if(access >= 27)
			menu.addButton(new Button("lvl_ml", "lvl_mc", "lvl_mr", "27", gotoLevel27));
		else 
			menu.addButton(new Button("nb_ml", "nb_mc", "nb_mr", "??"));
	
		menu.setPrevButton(new Button("hud_prev", "btn_center", "btn_right", GV.getText(327), gotoPrev));
		
		container.addChild(new Entity().add(menu));
		
		menu.setXY(	mapWidthInPx() / 2 - menu.getNaturalWidth()/2+GV.k/2, 
					mapHeightInPx()/2 - menu.getNaturalHeight()/2);
	}
	
	function gotoLevel19() { scenario.gotoScene(19); }
	function gotoLevel20() { scenario.gotoScene(20); }
	function gotoLevel21() { scenario.gotoScene(21); }
	function gotoLevel22() { scenario.gotoScene(22); }
	function gotoLevel23() { scenario.gotoScene(23); }
	function gotoLevel24() { scenario.gotoScene(24); }
	function gotoLevel25() { scenario.gotoScene(25); }
	function gotoLevel26() { scenario.gotoScene(26); }
	function gotoLevel27() { scenario.gotoScene(27); }
	
	function gotoPrev() {
		var scene = new HubScene2();
		scene.setScenario(scenario);
		var transition = new SlideTransition(1, Ease.quintInOut).down();
		GV.unwindToScene(scene.getScene(), transition); 
	}
	
}