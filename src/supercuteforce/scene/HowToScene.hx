package supercuteforce.scene;

import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.actions.VoidCondition;
import supercuteforce.gui.Dialog;
import supercuteforce.gui.ControlMenu;
import flambe.scene.Director;
import flambe.scene.FadeTransition;
import flambe.System;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class HowToScene extends GameScene
{
	var hasGun:Bool = false;
	var dlgOffset = 0;
	
	public function new() 
	{
		if(System.touch.supported == false)
			dlgOffset = 5;
		super("?", 2);
	}
	
	override function initRules():Void {
		setVictoryCondition(new VoidCondition());
		
		hasGun = Player.hasGun;
		if (hasGun == false) player.giveGun(true);
		
		super.initRules();
	}
	
	override function initDlg():Void 
	{
		var dialog:Dialog = new Dialog();
 		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(290+dlgOffset), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(291+dlgOffset), true);
		
		dialog.executeAtEnd(secondDlg);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(1), 				
									new CallFunction(dialog.nextTalk),
									new CallFunction(setToInactive)]));
	}
	
	function secondDlg() {
		var dialog:Dialog = new Dialog();
		showTextElement(GV.getText(300));
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(292+dlgOffset), true);
		
		dialog.executeAtEnd(thirdDlg);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(5), 				
									new CallFunction(dialog.nextTalk),
									new CallFunction(setToInactive)]));
	}
	
	function thirdDlg() {
		setActive(true);
		var dialog:Dialog = new Dialog();
		showTextElement(GV.getText(301));
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(293+dlgOffset), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(294), true);
		
		dialog.executeAtEnd(fourthDlg);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([new Delay(5), 				
									new CallFunction(dialog.nextTalk),
									new CallFunction(setToInactive)]));
	}
	
	
	function fourthDlg() {
		gotoPauseMenu();
	}
	
	override public function gotoPauseMenu() {	
		pause();
		setActive(false);
		var menu = new ControlMenu(this, false, false, false);
		var d:Director = GV.getRoot().get(Director);
		if (d != null) {
			d.pushScene(new Entity().add(menu), new FadeTransition(0.2));
		}
	}
	
	override public function dispose() {
		super.dispose();
		player.giveGun(hasGun);
	}
	
	function setToInactive() {setActive(false);}
}