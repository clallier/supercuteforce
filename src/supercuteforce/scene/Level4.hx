package supercuteforce.scene;
import supercuteforce.gui.Dialog;
import flambe.Entity;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.script.Delay;
import flambe.script.CallFunction; 
import supercuteforce.backgrounds.BoidsBackground;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level4 extends GameScene
{

	public function new(noDlg:Bool) 
	{
		super("4", 3, noDlg);
	}
	
	override function initRules():Void {
		hud.setBulletLeft(1);
		setBackground(new BoidsBackground());
		super.initRules();
	}
	
	override function initDlg():Void 
	{
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(32), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(33), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(34), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(35), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(36), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(37), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(38), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(39), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(40), true);
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() {
		var map = [
		[0, 0, 1, 1, 1, 1, 1, 0, 0],
		[0, 0, 1, 1, 1, 1, 9, 0, 0],
		[0, 0, 1, 1, 1, 1, 9, 0, 0],
		[0, 0, 1, 1, 1, 9, 9, 0, 0],
		[0, 0, 1, 1, 9, 9, 9, 0, 0],
		[0, 0, 1, 9, 9, 9, 9, 0, 0]];
		setMap(map);
		
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(41), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(42), false);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
}