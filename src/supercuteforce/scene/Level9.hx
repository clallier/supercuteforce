package supercuteforce.scene;
import supercuteforce.gui.Dialog;
import flambe.Entity;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.script.Delay;
import flambe.script.CallFunction;
import supercuteforce.blocks.BubblePink;
import flambe.System;
import supercuteforce.backgrounds.BoidsBackground;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level9 extends GameScene
{
	public function new(noDlg:Bool) {
		super("9", 3, noDlg);
	}
			
	override function initRules():Void {
		hud.setBulletLeft(8);
		BubblePink.setMuted(true);
		setBackground(new BoidsBackground());
		super.initRules();
	}
	
	override function initDlg():Void
	{
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(71), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(72), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(73), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(74), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(75), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(76), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(77), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(78), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(79), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(80), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(81), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(82), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(83), true);

		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() {
		
		var map = [
		[2, 1, 3, 1, 2, 1, 3, 1, 2],
		[2, 1, 1, 1, 2, 1, 1, 1, 2],
		[3, 2, 2, 2, 3, 2, 2, 2, 3],
		[3, 1, 2, 1, 3, 1, 2, 1, 3],
		[1, 1, 2, 1, 1, 1, 2, 1, 1],
		[0, 1, 3, 1, 9, 1, 3, 1, 0],
		[0, 3, 3, 3, 9, 3, 3, 3, 0],
		[0, 0, 0, 1, 1, 1, 0, 0, 0],
		[0, 0, 0, 0, 1, 0, 0, 0, 0]];
		
		setMap(map);
		
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(84), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(85), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(86), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(87), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(88), false);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}		
}