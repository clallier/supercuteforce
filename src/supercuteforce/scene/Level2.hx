package supercuteforce.scene;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.gui.Dialog;
import supercuteforce.backgrounds.BoidsBackground;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level2 extends GameScene
{
	public function new(noDlg:Bool) 
	{
		super("2", 3, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new BoidsBackground());
		super.initRules();
	}
	
	override function initDlg():Void {
			var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(17), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(18), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(19), true);
	
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}

	override public function buildScene() 
	{
		var map = [
		[1, 2, 2, 2, 0, 1, 1, 1, 2],
		[1, 1, 2, 1, 0, 2, 1, 2, 2],
		[0, 1, 0, 1, 2, 2, 0, 2, 0],
		[0, 0, 0, 1, 2, 0, 0, 0, 0],
		[0, 0, 0, 1, 0, 0, 0, 0, 0]];
	
		setMap(map);
		super.buildScene();
	}
	
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(20), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(21), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(22), false);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
}