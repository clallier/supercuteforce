package supercuteforce.scene;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.actions.ScoreCondition;
import supercuteforce.backgrounds.ColorCityBackground;
import supercuteforce.blocks.BubblePink;
import supercuteforce.gui.Dialog;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level11 extends GameScene
{
	public function new(noDlg:Bool) {
		super("11", 1, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new ColorCityBackground());
		setVictoryCondition(new ScoreCondition(this, 1500));
		BubblePink.setMuted(true);
		super.initRules();
	}
	
	override function initDlg():Void 
	{
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(99), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(100), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(101), true);
		
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() {
		initNewLines(8, 3);
		container.add(new Generator(this, 8));
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(102), false);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(103), false);
	
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
}