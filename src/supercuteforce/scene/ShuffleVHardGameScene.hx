package supercuteforce.scene;
import supercuteforce.Generator;
import supercuteforce.actions.ScoreCondition;
import supercuteforce.blocks.BlockFactory.BlockType;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class ShuffleVHardGameScene extends ShuffleGameScene
{

	public function new(noDlg:Bool) 
	{
		super("!!!!", 3, noDlg);
	}
	
	override function initRules():Void {
		setVictoryCondition(new ScoreCondition(this, 10000));
		super.initRules();
	}
	
	override public function buildScene() {
		initNewLines(11, 6);
		createAndAddBlock(0, 0, BlockType.snake_head);
		createAndAddBlock(GV.mapWidth-1, 0, BlockType.snake_head);
		container.add(new Generator(this, 15));
		
		super.buildScene();
	}
}