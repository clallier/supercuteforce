package supercuteforce.scene;
import supercuteforce.actions.ScoreCondition;
import supercuteforce.blocks.BubblePink;
import supercuteforce.Generator;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class ShuffleEasyGameScene extends ShuffleGameScene
{

	public function new(noDlg:Bool) 
	{
		super("!", 1, noDlg);
	}
	
	override function initRules():Void {
		BubblePink.setMuted(true);
		setVictoryCondition(new ScoreCondition(this, 10000));
		super.initRules();
	}
	
	override public function buildScene() {
		initNewLines(8, 5);
		container.add(new Generator(this, 20));
		
		super.buildScene();
	}
}