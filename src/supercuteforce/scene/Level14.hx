package supercuteforce.scene;

import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.actions.ScoreCondition;
import supercuteforce.backgrounds.ColorCityBackground;
import supercuteforce.blocks.BubblePink;
import supercuteforce.gui.Dialog;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level14 extends GameScene
{
	public function new(noDlg:Bool) {
		super("14", 1, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new ColorCityBackground());
		BubblePink.setMuted(true);
		setVictoryCondition(new ScoreCondition(this, 1800));
		super.initRules();
	}
	
	override function initDlg():Void
	{
		
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(118), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(119), false);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(120), true);
		
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() 
	{
		initNewLines(9, 4);
		container.add(new Generator(this, 7));
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(121), false);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(122), false);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(123), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(124), false);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(125), false);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
}