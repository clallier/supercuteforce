package supercuteforce.scene;
import supercuteforce.gui.Dialog;
import flambe.Entity;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.script.Delay;
import flambe.script.CallFunction;
import supercuteforce.blocks.BubblePink;
import supercuteforce.backgrounds.BoidsBackground;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level7 extends GameScene
{
	public function new(noDlg:Bool) 
	{
		super("7", 3, noDlg);	
	}
		
	override function initRules():Void {
		hud.setBulletLeft(6);
		BubblePink.setMuted(true);
		setBackground(new BoidsBackground());
		super.initRules();
	}
	
	override function initDlg():Void
	{
		var dialog:Dialog = new Dialog();
		
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(60), true);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(61), true);
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() {
		var map = [
		[3, 3, 3, 2, 2, 2, 3, 3, 3],
		[3, 1, 1, 1, 2, 1, 1, 1, 3],
		[0, 1, 0, 3, 3, 3, 0, 1, 0],
		[2, 2, 2, 2, 3, 2, 2, 2, 2],
		[1, 1, 1, 1, 3, 1, 1, 1, 1],		
		[0, 0, 9, 0, 1, 0, 9, 0, 0],
		[0, 0, 9, 9, 1, 9, 9, 0, 0],
		[0, 0, 0, 9, 1, 9, 0, 0, 0],
		[0, 0, 0, 0, 1, 0, 0, 0, 0]
		];
		setMap(map);
		
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(62), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(63), false);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(64), true);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}	
}