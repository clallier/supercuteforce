package supercuteforce.scene;

import flambe.display.TextSprite;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Sequence;
import supercuteforce.blocks.Block;
import flambe.Entity;
import supercuteforce.gui.Dialog;
import supercuteforce.gui.TalkBubble;
import flambe.script.Script;
import supercuteforce.backgrounds.BoidsBackground;
import flambe.System;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level1 extends GameScene
{	
	public function new(noDlg:Bool) 
	{
		super("1", 3, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new BoidsBackground());
		super.initRules();
	}
	
	override function initDlg():Void {
		var dialog:Dialog = new Dialog();
		
	
		// keyboard
		if (System.touch.supported == false) {
			dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(295), true);
			dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(296), true);
			dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(297), true);
		}
		// touchscreen
		else {
			dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(290), true);
			dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(291), true);
			dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(292), true);
		}
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(293), true);
		
		// story 
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(1), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(2), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(3), true);
		dialog.addTalk(GV.imgKawaiGun, GV.nameNewItems, GV.getText(7), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(8), true);
		
		//intro
		// "1" : "Hello and welcome in our kingdom!",
		// "2" : "Oh! The Gods Of Cuteness!",
		// "3" : "Emerald Princess! We invoked you to serve us! Zombie Candies invade our kingdom!\nTake this to help you in your quest!",
		// "7" : "You got the Kawaii Gun!",
		// "8" : "You have to make chains to prove your valour!",
		
	//touch
	// "291" : "Nice! Now to move, just touch the left or right side of the princess.",
	// "292" : "Well done! To shoot, touch the princess. Warning! The gun has a cooldown!",
	
    // keyboard
	// "295" : "Hello and welcome to Super Cute Force.
   // "296" : "Nice! Now to move, just use [LEFT ARROW] and [RIGHT ARROW].",
	//"297" : "Well done! To shoot, use [SPACE]. Warning! The gun has a cooldown!",
	// 298 :" The story begins!"
		
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() {
		player.giveGun(true);
		var map = [
		[0, 1, 0, 0, 0, 1, 1, 0, 0],
		[0, 1, 1, 0, 0, 0, 1, 0, 0],
		[0, 0, 1, 0, 0, 0, 1, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0]];
		
		setMap(map);
		
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(10), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(11), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(12), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(13), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(14), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(15), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(16), false);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
}