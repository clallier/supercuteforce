package supercuteforce.scene;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.backgrounds.TokyoCityBackground;
import supercuteforce.gui.Dialog;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level22 extends GameScene
{
		
	public function new(noDlg:Bool) {
		super("22", 2, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new TokyoCityBackground());
		super.initRules();
	}
	
	override function initDlg():Void
	{
		
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(207), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(208), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(209), false);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(210), true);
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() 
	{
		initNewLines(10, 6);
		container.add(new Generator(this, 13));
		super.buildScene();
		
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(211), false);

		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
}