package supercuteforce.scene;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.actions.ScoreCondition;
import supercuteforce.backgrounds.TokyoCityBackground;
import supercuteforce.gui.Dialog;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level23 extends GameScene
{

	public function new(noDlg:Bool) {
		super("23", 1, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new TokyoCityBackground());
		setVictoryCondition(new ScoreCondition(this, 2500));
		super.initRules();
	}
	
	override function initDlg():Void
	{
		
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(215), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(216), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(217), true);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(218), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(219), true);
		dialog.addTalk(GV.imgMaskedMa, GV.nameMaskedMa, GV.getText(220), true);
		dialog.addTalk(GV.imgLuckyGod, GV.nameLuckyGod, GV.getText(221), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(222), true);
		dialog.addTalk(GV.imgLuckyGod, GV.nameLuckyGod, GV.getText(223), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(224), true);
		dialog.addTalk(GV.imgLuckyGod, GV.nameLuckyGod, GV.getText(225), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(226), true);
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() 
	{
		initNewLines(10, 6);
		container.add(new Generator(this, 5));
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(227), false);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
}