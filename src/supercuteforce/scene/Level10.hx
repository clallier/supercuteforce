package supercuteforce.scene;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.System;
import supercuteforce.backgrounds.ColorCityBackground;
import supercuteforce.blocks.BubblePink;
import supercuteforce.gui.Dialog;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level10 extends GameScene
{
	public function new(noDlg:Bool) {
		super("10", 2, noDlg);
	}
		
	override function initRules():Void {
		BubblePink.setMuted(true);
		setBackground(new ColorCityBackground());
		System.storage.set(GV.keyEasy, true);
		super.initRules();
	}
		
	override function initDlg():Void
	{
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgTheGifts, GV.nameNewItems, GV.getText(89), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(90), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(91), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(92), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(93), true);
		
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	override public function buildScene() {
		initNewLines(7, 3);
		container.add(new Generator(this, 15));
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(94), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(95), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(96), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(97), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(98), true);
		dialog.executeAtEnd(showVictoryMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
}