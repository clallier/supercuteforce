package supercuteforce.scene;
import flambe.animation.Ease;
import flambe.asset.AssetPack;
import flambe.asset.Manifest;
import flambe.display.FillSprite;
import flambe.Entity;
import flambe.System;
import flambe.util.Promise;
import supercuteforce.gui.Scenario;
import supercuteforce.GV;
/**
 * ...
 * @author Godjam - QilinEggs
 */
class PreloaderScene
{
	var scene: Entity;
	var loaderTextures : Promise<AssetPack>;
	var loaderBootstrap : Promise<AssetPack>;
	var progressWidth = 100;
	var globalScenario:Scenario;
	var progress = 0.0;
	var total = 0.0;
	var barSprite:FillSprite;
	var barBgSprite:FillSprite;
	var bgSprite:FillSprite;
	
	public function new ()
	{
		scene = new Entity();
		// bg 
		bgSprite = new FillSprite(0xffffff, GV.width, GV.height);
		scene.addChild(new Entity().add(bgSprite));
		//progress bar
		progressWidth = Std.int(200*(GV.k/32));
		var progressBar = new FillSprite(0xEEFFA3, progressWidth, 20);
		scene.addChild(new Entity().add(progressBar));
		progressBar.centerAnchor();
		progressBar.setXY(GV.width/2, GV.height/2);
        
		barSprite = new FillSprite(0x95ffb3, 0, 20);
		barSprite.centerAnchor();
		scene.addChild(new Entity().add(barSprite));
		barSprite.setXY(GV.width/2, GV.height/2);
		barSprite.alpha.animateTo(1, 1);
		
		// bootstrap loader
		var manifestBootstrap = Manifest.fromAssets("bootstrap");
		loaderBootstrap = System.loadAssetPack(manifestBootstrap);
		loaderBootstrap.get(onFinishLoadBootstrap);
		loaderBootstrap.progressChanged.connect(onProgress);
		
		var manifestTextures = Manifest.fromAssets("textures" + GV.k);
		loaderTextures= System.loadAssetPack(manifestTextures);
		loaderTextures.get(onFinishLoadTextures);
		loaderTextures.progressChanged.connect(onProgress);
	
		//scenario
		globalScenario = new Scenario();
		trace("load scene loaded");
	}

	function onFinishLoadBootstrap(pack:AssetPack) {
		GV.setBPack(pack);
		if(GV.tPack != null && GV.bPack != null)
			globalScenario.gotoMainMenu();
	}
	
	function onFinishLoadTextures(pack:AssetPack) {
		GV.setTPack(pack);
		if(GV.tPack != null && GV.bPack != null)
			globalScenario.gotoMainMenu();
	}
	
	function onProgress() {
		
		var ratio = 0.0;
		progress = loaderBootstrap.progress + loaderTextures.progress;
		total = loaderBootstrap.total + loaderTextures.total;
		
		if (total != 0) 
			ratio = progress / total*1.33;
		
		if (ratio > 0.99) {
			ratio = 1;
			barSprite.color = 0x95ffb3;
			barSprite.scaleX.animateTo(100, 5, Ease.cubeIn);
			barSprite.scaleY.animateTo(100, 5, Ease.cubeIn);
			//barSprite.rotation.animateTo(360, 5, Ease.cubeOut);
		}
		
		barSprite.width._ = ratio * progressWidth;
		barSprite.centerAnchor();

	}
	
	public function getScene() {
		return scene;
	}
	
}