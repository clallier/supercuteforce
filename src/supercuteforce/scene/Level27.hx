package supercuteforce.scene;
import flambe.Entity;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Script;
import flambe.script.Sequence;
import flambe.System;
import supercuteforce.backgrounds.ColorCityBackground;
import supercuteforce.gui.Dialog;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Level27 extends GameScene
{
	public function new(noDlg:Bool) {
		super("27", 3, noDlg);
	}
	
	override function initRules():Void {
		setBackground(new ColorCityBackground());
		super.initRules();
	}
	
	override function initDlg():Void
	{
		
		var dialog:Dialog = new Dialog();

		dialog.addTalk(GV.imgLuckyGod, GV.nameLuckyGod, GV.getText(260), true);
		dialog.addTalk(GV.imgGodsCut2, GV.nameGodsCute, GV.getText(261), true);
		
		dialog.executeAtEnd(buildSecondDlg);
		container.addChild(new Entity().add(dialog));
		
		var script = container.get(Script);
		if (script != null)
		script.run(new Sequence([	new Delay(2), 				
									new CallFunction(dialog.nextTalk)]));
	}
	
	function buildSecondDlg() 
	{
		shakeScreen(40);
		GV.bPack.getSound("quake").play();
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgLuckyGod, GV.nameLuckyGod, GV.getText(262), true);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(263), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(264), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(265), true);
		dialog.executeAtEnd(buildScene);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
	override public function buildScene() 
	{
		var map = [
		[7, 2, 1, 1, 1, 1, 1, 2, 7],
		[3, 3 ,1, 1, 3 ,1, 1, 3 ,3],
		[3, 2, 2, 3, 3, 3, 2, 2, 3],
		[3, 4, 4, 3, 5, 3, 4, 4, 3],
		[2, 2, 2, 3, 5, 3, 2, 2, 2],
		[3, 9, 5, 4, 5, 4, 5, 9, 3],
		[3, 1, 4, 4, 4, 4, 4, 1, 3],
		[3, 9, 1, 1, 1, 1, 1, 9, 3],
		[3, 2, 3, 3, 3, 3, 3, 2, 3]];
		setMap(map);
		container.add(new Generator(this, 12));
		super.buildScene();
	}
	
	override function runEndWithVictory() 
	{
		setActive(false);
		System.storage.set(GV.keyHard, true);
		System.storage.set(GV.keyVHard, true);
		var dialog:Dialog = new Dialog();
		
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(266), false);
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(267), true);
		dialog.addTalk(GV.imgTheGifts, GV.nameNewItems, GV.getText(268), false);
		dialog.addTalk(GV.imgTheGifts, GV.nameNewItems, GV.getText(269), false);
		dialog.executeAtEnd(gotoMainMenu);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
	
	override function runEndWithLose() 
	{
		setActive(false);
		var dialog:Dialog = new Dialog();
		dialog.addTalk(GV.imgGodsCute, GV.nameGodsCute, GV.getText(270), true);
		dialog.addTalk(GV.imgPrincess, GV.namePrincess, GV.getText(271), false);
		dialog.executeAtEnd(showLoseMessageAndGotoNext);
		container.addChild(new Entity().add(dialog));
		dialog.nextTalk();
	}
}