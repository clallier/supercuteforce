package supercuteforce.scene;
import supercuteforce.Generator;
import supercuteforce.actions.ScoreCondition;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class ShuffleHardGameScene extends ShuffleGameScene
{

	public function new(noDlg:Bool) 
	{
		super("!!!", 2, noDlg);
	}
	
	override function initRules():Void {
		setVictoryCondition(new ScoreCondition(this, 10000));
		super.initRules();
	}
	
	override public function buildScene() {
		initNewLines(10, 6);
		container.add(new Generator(this, 16));
		
		super.buildScene();
	}
}