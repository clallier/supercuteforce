package supercuteforce;

import flambe.Entity;
import flambe.script.Action;
import flambe.script.CallFunction;
import flambe.script.Delay;
import flambe.script.Repeat;
import flambe.script.Script;
import flambe.script.Sequence;
import supercuteforce.blocks.BlockFactory;
import supercuteforce.scene.GameScene;
import supercuteforce.actions.PopNewLine;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Generator extends Script
{
	var scene:GameScene;
	
	public function new(scene:GameScene, cooldown:Int) {
		super();
		this.scene = scene;
		
		//trace("new generator");
		
		run(new Repeat(new Sequence([
			new Delay(cooldown),
			new CallFunction(execute)]))); 
	}
	
	function execute() {
		scene.addAction(new PopNewLine(scene));
	}
}