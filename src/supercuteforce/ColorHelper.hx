package supercuteforce;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class ColorHelper
{
/**
 * ...
 * @author Godjam - QilinEggs
 */
	private static function hsv2Hex(h:Int, s:Int, v:Int): Int
	{
		var i:Int = 0;
		var f=0, p=0, q=0, t=0;
		var r=0, g=0, b=0;
		
		if( s == 0 ) {
			// achromatic (grey)
			r = g = b = v;
			return 0x555555;
		}
		
		h = Std.int(h/60);			// sector 0 to 5
		i = Math.floor( h );
		f = h - i;			// factorial part of h
		p = v * ( 1 - s );
		q = v * ( 1 - s * f );
		t = v * ( 1 - s * ( 1 - f ) );
		
		if( i == 0) {
			r = v;
			g = t;
			b = p;
		}
		
		if ( i == 1 ) {
			r = q;
			g = v;
			b = p;
		}
		
		if ( i == 2 ) {
			r = p;
			g = v;
			b = t;
		}
		
		if ( i == 3 ) {
			r = p;
			g = q;
			b = v;
		}
		
		if ( i == 4 ) {
			r = t;
			g = p;
			b = v;
		}
	
		if ( i == 5 ) {
			r = v;
			g = p;
			b = q;
		}
	
		var hexColor = (r & 0xFF) << 16 | (g & 0xFF) << 8 | (b & 0xFF);
		return hexColor;
	}
	
	/**
	 * change the screen color using a random color
	 */
	public static function getRandomColor():Int 
	{
		var h:Int = Std.int( Math.random() * 360 + 1);
		var color = hsv2Hex(h,255,110);
		return color;
	}
	
}